<?php

namespace Drupal\crossword;

use Drupal\file\FileInterface;

/**
 * Provides an interface for crossword file parser.
 */
interface CrosswordFileParserPluginInterface {

  /**
   * Determine whether the parser works for the input file.
   *
   * This should be a quick and simple test of the basic
   * and essential structure of the file. It could be as
   * simple as checking the extension.
   *
   * @param \Drupal\file\FileInterface $file
   *   The user input file to test against the plugins.
   *
   * @return bool
   *   Returns TRUE if the plugin can parse the input file.
   */
  public static function isApplicable(FileInterface $file);

  /**
   * Returns the data array representing the parsed crossword file.
   *
   * If the file has structural problems, this function should throw
   * a CrosswordException. This will prevent corrupted files from
   * getting uploaded, even if they pass the simple validation of
   * ::isApplicable().
   *
   * The "data" array is used to do interesting things in Drupal.
   * The idea is tp convert any uploadded file of any supported
   * file type into this common structure.
   *
   * See crossword/tests/files/test.json for example json.
   *
   * Array(
   *   'id' => $this->file->id(),
   *   'title' => 'Awesome Puzzle',
   *   'author' => 'Dan',
   *   'notepad' => 'These are notes included in the file',
   *   'puzzle' => [
   *     'grid' => array of squares
   *     'clues' => [
   *       'across' => array of clues
   *       'down' => array of clues
   *     ],
   *   ],
   * )
   *
   * A square looks like this...
   * array(
   *  'fill' => NULL or a string,
   *  'numeral' => NULL or a number,
   *  'across' => [
   *    'index' => index of across clue
   *  ],
   *  'down' => [
   *    'index' => index of down clue
   *  ],
   *  'moves' => [
   *    'up' => ['row': number, 'col': number] or NULL
   *    'down' => ['row': number, 'col': number] or NULL
   *    'left' => ['row': number, 'col': number] or NULL
   *    'right' => ['row': number, 'col': number] or NULL
   *  ],
   *  'circle' => bool,
   *  'rebus' => bool,
   *  'image' => [
   *     'format' => jpg or png or whatever
   *     'data' => base64 encoded data of the image
   *     'width' => number of squares wide
   *     'height' => number of squares tall
   *  ]
   * )
   *
   * Moves get added in the crossword data service.
   *
   * A clue looks like this...
   * array(
   *  'text' => string,
   *  'numeral' => number,
   *  'references' => array(
   *    [
   *      'dir' => 'down' or 'across',
   *      'numeral' => number,
   *      'index' => number,
   *    ],
   *   )
   * )
   *
   * References get added in the crossword data service.
   *
   * @return array
   *   An associative array that represents the crossword.
   *
   * @throws \Drupal\crossword\CrosswordException
   *   Thrown if something causes parsing to fail.
   */
  public function parse();

}
