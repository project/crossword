<?php

namespace Drupal\crossword;

use Drupal\file\FileInterface;

/**
 * Interface for CrosswordDataService.
 */
interface CrosswordDataServiceInterface {

  /**
   * Get structured crossword data from a file.
   *
   * See CrosswordFilePluginInterface::parse() for returned data structure.
   *
   * @param Drupal\file\FileInterface $file
   *   The (crossword) file.
   * @param bool $redacted
   *   If TRUE, answers and rebus info will not be included.
   *
   * @return mixed
   *   The structured data.
   */
  public function getData(FileInterface $file, $redacted = FALSE);

  /**
   * Remove any spoilers from the data (fill and rebus).
   *
   * The convention is that when 'fill' is NULL, the square is black.
   * Redaction changes any non-NULL fill to an empty string. Therefore it
   * may be dangerous to use == for comparisons, since both the empty string
   * and NULL are falsey.
   *
   * @param array $data
   *   Data as returned by ::getData().
   *
   * @return array
   *   The redacted data.
   */
  public function redact(array $data);

  /**
   * Get the crossword author from a file.
   *
   * @param Drupal\file\FileInterface $file
   *   The (crossword) file.
   *
   * @return mixed
   *   Return author if available.
   */
  public function getAuthor(FileInterface $file);

  /**
   * Get the crossword dimension from a file.
   *
   * @param Drupal\file\FileInterface $file
   *   The (crossword) file.
   *
   * @return mixed
   *   Return number of columns if available.
   */
  public function getDimensionAcross(FileInterface $file);

  /**
   * Get the crossword dimension from a file.
   *
   * @param Drupal\file\FileInterface $file
   *   The (crossword) file.
   *
   * @return mixed
   *   Return number of rows if available.
   */
  public function getDimensionDown(FileInterface $file);

  /**
   * Get the crossword dimensions from a file.
   *
   * @param Drupal\file\FileInterface $file
   *   The (crossword) file.
   * @param string $delimiter
   *   The delimiter.
   *
   * @return mixed
   *   Return number of dimensions if available.
   */
  public function getDimensions(FileInterface $file, $delimiter = 'x');

  /**
   * Get the crossword title from a file.
   *
   * @param Drupal\file\FileInterface $file
   *   The (crossword) file.
   *
   * @return mixed
   *   Return title of crossword if available. (Not the file name!)
   */
  public function getTitle(FileInterface $file);

  /**
   * Get the solution to the puzzle.
   *
   * For the best usability, the format of this array should match
   * the js Drupal.Crossword.Crossword::getAnswers() function. That
   * means that the correct answer for a black square is an empty
   * string, not NULL.
   *
   * @param Drupal\file\FileInterface $file
   *   The (crossword) file.
   * @param string $black
   *   The string to represent a black square.
   *
   * @return array|null
   *   Puzzle solution.
   */
  public function getSolution(FileInterface $file, $black = '');

  /**
   * Find out if it's a rebus puzzle, which may require more instructions.
   *
   * @param Drupal\file\FileInterface $file
   *   The (crossword) file.
   *
   * @return bool
   *   True if it's a rebus crossword puzzle.
   */
  public function isRebus(FileInterface $file);

}
