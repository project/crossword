<?php

namespace Drupal\crossword\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Crossword Instructions block.
 *
 * @Block(
 *   id = "crossword_instructions",
 *   admin_label = @Translation("Crossword Instructions")
 * )
 */
class CrosswordInstructionsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'cheat' => TRUE,
      'rebus' => TRUE,
      'errors' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['cheat'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show keyboard shortcut for Cheat'),
      '#default_value' => $this->configuration['cheat']
    ];
    $form['rebus'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show keyboard shortcut for Rebus'),
      '#default_value' => $this->configuration['rebus'],
    ];
    $form['errors'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show keyboard shortcut for Toggle Errors'),
      '#default_value' => $this->configuration['errors'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['cheat'] = $form_state->getValue('cheat');
    $this->configuration['rebus'] = $form_state->getValue('rebus');
    $this->configuration['errors'] = $form_state->getValue('errors');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'crossword_instructions',
      '#cheat' => $this->configuration['cheat'],
      '#rebus' => $this->configuration['rebus'],
      '#errors' => $this->configuration['errors'],
      '#field_formatter' => 'block',
      '#attached' => [
        'library' => [
          'crossword/crossword.instructions',
        ],
      ],
    ];
  }

}
