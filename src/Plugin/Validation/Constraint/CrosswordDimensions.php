<?php

namespace Drupal\crossword\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the grid is within the allowed dimensions.
 *
 * @Constraint(
 *   id = "CrosswordDimensions",
 *   label = @Translation("Crossword Dimensions", context = "Validation"),
 *   type = "file"
 * )
 */
class CrosswordDimensions extends Constraint {

  /**
   * Too many columns.
   *
   * @var string
   */
  public $tooManyColumns = 'That puzzle has %columns columns, which is more than the allowed %allowed.';

  /**
   * Too many rows.
   *
   * @var string
   */
  public $tooManyRows = 'That puzzle has %columns rows, which is more than the allowed %allowed.';

  /**
   * Too few columns.
   *
   * @var string
   */
  public $tooFewColumns = 'That puzzle has %columns columns, which is fewer than the required %required.';

  /**
   * Too few rows.
   *
   * @var string
   */
  public $tooFewRows = 'That puzzle has %row rows, which is fewer than the required %required.';

  /**
   * {@inheritdoc}
   */
  public function validatedBy(): string {
    return '\Drupal\crossword\Plugin\Validation\Constraint\CrosswordDimensionsValidator';
  }

}
