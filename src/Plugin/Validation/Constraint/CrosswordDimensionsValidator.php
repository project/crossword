<?php

namespace Drupal\crossword\Plugin\Validation\Constraint;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\crossword\CrosswordDataServiceInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Validates the crossword size.
 */
class CrosswordDimensionsValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Crossword data service.
   *
   * @var \Drupal\crossword\CrosswordDataServiceInterface
   */
  protected $crosswordDataService;

  /**
   * File storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fileStorage;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Create an instance of the validator.
   *
   * @param \Drupal\crossword\CrosswordDataServiceInterface $crossword_data_service
   *   The parser manager service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   Entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(CrosswordDataServiceInterface $crossword_data_service, EntityTypeManager $entity_type_manager, LoggerInterface $logger) {
    $this->crosswordDataService = $crossword_data_service;
    $this->fileStorage = $entity_type_manager->getStorage('file');
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('crossword.data_service'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')->get('crossword')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    $max_columns = $items->getFieldDefinition()->getSetting('max_columns');
    $min_columns = $items->getFieldDefinition()->getSetting('min_columns');
    $max_rows = $items->getFieldDefinition()->getSetting('max_rows');
    $min_rows = $items->getFieldDefinition()->getSetting('min_rows');

    foreach ($items as $item) {
      if (get_class($item) == "Drupal\Core\TypedData\Plugin\DataType\IntegerData") {
        $file = $this->fileStorage->load($item->getCastedValue());
        // The CrosswordFile validator has already run. That means we can safely
        // rely on the data service.
        $columns = $this->crosswordDataService->getDimensionAcross($file);
        $rows = $this->crosswordDataService->getDimensionDown($file);
        if ($max_columns && $columns > $max_columns) {
          $this->context->addViolation($constraint->tooManyColumns, ['%columns' => $columns, '%allowed' => $max_columns]);
        }
        if ($min_columns && $columns < $min_columns) {
          $this->context->addViolation($constraint->tooFewColumns, ['%columns' => $columns, '%required' => $min_columns]);
        }
        if ($max_rows && $rows > $max_rows) {
          $this->context->addViolation($constraint->tooManyRows, ['%rows' => $rows, '%allowed' => $max_rows]);
        }
        if ($min_rows && $rows < $min_rows) {
          $this->context->addViolation($constraint->tooFewRows, ['%rows' => $rows, '%required' => $min_rows]);
        }
      }
    }
  }

}
