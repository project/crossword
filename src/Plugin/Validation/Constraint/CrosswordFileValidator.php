<?php

namespace Drupal\crossword\Plugin\Validation\Constraint;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\crossword\CrosswordException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\crossword\CrosswordFileParserManagerInterface;

/**
 * Validates the crossword file.
 */
class CrosswordFileValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Crossword Parser manager service.
   *
   * @var \Drupal\crossword\CrosswordFileParserManagerInterface
   */
  protected $parserManager;

  /**
   * File storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fileStorage;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Create an instance of the validator.
   *
   * @param \Drupal\crossword\CrosswordFileParserManagerInterface $parser_manager
   *   The parser manager service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   Entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(CrosswordFileParserManagerInterface $parser_manager, EntityTypeManager $entity_type_manager, LoggerInterface $logger) {
    $this->parserManager = $parser_manager;
    $this->fileStorage = $entity_type_manager->getStorage('file');
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('crossword.manager.parser'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')->get('crossword')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {

    $allowed_parsers = $items->getFieldDefinition()->getSetting('allowed_parsers');
    $allowed_parser_definitions = $this->parserManager->loadDefinitionsFromOptionList($allowed_parsers);

    foreach ($items as $item) {
      if (get_class($item) == "Drupal\Core\TypedData\Plugin\DataType\IntegerData") {
        $file = $this->fileStorage->load($item->getCastedValue());
        if ($definition = $this->parserManager->filterApplicableDefinitions($allowed_parser_definitions, $file)) {
          /** @var $parser \Drupal\crossword\CrosswordFileParserPluginInterface */
          $parser = $this->parserManager->createInstance($definition['id'], ['fid' => $file->id()]);
          try {
            $data = $parser->parse();
          }
          catch (CrosswordException $e) {
            $this->logger->error('The %parser parser failed parsing file with id %id: %message', ['%parser' => $parser->getPluginId(), '%id' => $file->id(), '%message' => $e->getMessage()]);
            $this->context->addViolation($constraint->corrupted, ['%parser' => $definition['title'], '%message' => $e->getMessage()]);
          }
        }
        else {
          $this->context->addViolation($constraint->noParser);
        }
      }
    }
  }

}
