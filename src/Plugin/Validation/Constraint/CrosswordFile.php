<?php

namespace Drupal\crossword\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the file can be paresed as a crossword puzzle.
 *
 * @Constraint(
 *   id = "CrosswordFile",
 *   label = @Translation("Crossword File", context = "Validation"),
 *   type = "file"
 * )
 */
class CrosswordFile extends Constraint {

  /**
   * A message when there is no valid parser.
   *
   * @var string
   */
  public $noParser = 'That does not appear to be a supported Crossword Puzzle file format.';

  /**
   * A message when a valid parser cannot parse the file.
   *
   * @var string
   */
  public $corrupted = 'The file format was identified as %parser, but the file reader was unsuccessful. The file may be corrupted or contain unsupported features. The message was: %message';

  /**
   * {@inheritdoc}
   */
  public function validatedBy(): string {
    return '\Drupal\crossword\Plugin\Validation\Constraint\CrosswordFileValidator';
  }

}
