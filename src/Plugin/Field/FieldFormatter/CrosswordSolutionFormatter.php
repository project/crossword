<?php

namespace Drupal\crossword\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'crossword_solution' formatter.
 *
 * @FieldFormatter(
 *   id = "crossword_solution",
 *   label = @Translation("Crossword Solution"),
 *   weight = "3",
 *   field_types = {
 *     "crossword"
 *   }
 * )
 */
class CrosswordSolutionFormatter extends CrosswordFormatter {

  const BUTTONS = [];

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options['details'] = [
      'title_tag' => 'h1',
      'author_tag' => 'h2',
      'notepad_tag' => 'p',
    ];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    unset($form['print']);
    unset($form['redacted']);
    unset($form['redacted_warning']);
    unset($form['congrats']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {

      $data = $this->crosswordDataService->getData($file);

      $elements[$delta] = [
        '#theme' => 'crossword',
        '#field_formatter' => $this->getPluginId(),
        '#content' => [
          'title' => $this->getTitle($data),
          'author' => $this->getAuthor($data),
          'notepad' => $this->getNotepad($data),
          'grid' => $this->getGrid($file, TRUE),
        ],
        '#attached' => [
          'library' => [
            'crossword/crossword.solution',
          ],
        ],
        '#attributes' => [],
        '#cache' => [
          'tags' => $file->getCacheTags(),
        ],
      ];
    }
    return $elements;
  }

}
