<?php

namespace Drupal\crossword\Plugin\Field\FieldType;

use Drupal\file\Plugin\Field\FieldType\FileItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'crossword' field type.
 *
 * @FieldType(
 *   id = "crossword",
 *   label = @Translation("Crossword"),
 *   description = @Translation("Field to upload crossword puzzle files."),
 *   default_widget = "file_generic_crossword",
 *   default_formatter = "file_default_crossword",
 *   list_class = "\Drupal\file\Plugin\Field\FieldType\FileFieldItemList",
 *   constraints = {
 *     "ReferenceAccess" = {},
 *     "FileValidation" = {},
 *     "CrosswordFile" = {},
 *     "CrosswordDimensions" = {},
 *   },
 *   cardinality = "1"
 * )
 */
class CrosswordItem extends FileItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'file_extensions' => 'txt puz xml ipuz',
      'allowed_parsers' => [],
      'max_columns' => NULL,
      'min_columns' => NULL,
      'max_rows' => NULL,
      'min_rows' => NULL,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::fieldSettingsForm($form, $form_state);
    $form['allowed_parsers'] = [
      '#title' => $this->t('Allowed Crossword File Parsers'),
      '#description' => $this->t('Make only selected parsers available. If none are selected any crossword file parser can be used.'),
      '#type' => 'checkboxes',
      '#default_value' => $this->getSetting('allowed_parsers'),
      '#options' => \Drupal::service('crossword.manager.parser')->getInstalledParsersOptionList(),
    ];
    $form['max_columns'] = [
      '#title' => $this->t('Maximum number of columns allowed'),
      '#description' => $this->t('This will be ignored if it is blank or zero.'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('max_columns'),
      '#min' => 0,
    ];
    $form['min_columns'] = [
      '#title' => $this->t('Minimum number of columns required'),
      '#description' => $this->t('This will be ignored if it is blank or zero.'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('min_columns'),
      '#min' => 0,
    ];
    $form['max_rows'] = [
      '#title' => $this->t('Maximum number of rows allowed'),
      '#description' => $this->t('This will be ignored if it is blank or zero.'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('max_rows'),
      '#min' => 0,
    ];
    $form['min_rows'] = [
      '#title' => $this->t('Minimum number of rows required'),
      '#description' => $this->t('This will be ignored if it is blank or zero.'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('min_rows'),
      '#min' => 0,
    ];
    return $form;
  }

}
