<?php

namespace Drupal\crossword;

/**
 * Defines the crossword exception class.
 */
class CrosswordException extends \Exception {

}
