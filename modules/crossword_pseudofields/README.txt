CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

 This module provides pseudofields for Crosswords on Nodes and Media.
 - Title
 - Author
 - Notepad
 - Controls
 - Active Clue
 - Grid
 - Clues Across
 - Clues Down
 - Make it Playable/Interactive

 This module is intended to make it easier to use Layout Builder. It also
 gives you more options when creating teaser view modes and you just want
 to include something like the Crossword title or author.

REQUIREMENTS
------------

 It requires its parent module (crossword) and node.

RECOMMENDED MODULES
-------------------

 This module is particularly useful if you want to make your own layout
 for the crossword puzzle using Layout Builder.

INSTALLATION
------------

 Install as usual, see https://www.drupal.org/node/1897420 for further
 information.

CONFIGURATION
-------------

 Pseudofields cannot be configured like normal fields, so this module provides
 a special configuration form to select how to render the various pseudofields.

 See /admin/config/content/crossword-pseudofields

TROUBLESHOOTING
---------------

 To make a puzzle playable in the browser using pseudofields, at a MINIMUM you
 must display these fields:

  - Grid
  - Make it Playable/Interactive
  - Active Clues

  or

  - Grid
  - Make it Playable/Interactive
  - Clues Across
  - Clues Down

 Without those, the puzzle isn't playable.

MAINTAINERS
-----------

 danflanagan8 (crossword enthusiast)
