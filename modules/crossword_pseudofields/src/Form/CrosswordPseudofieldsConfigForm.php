<?php

namespace Drupal\crossword_pseudofields\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure crossword pseudofields settings for this site.
 *
 * @internal
 */
class CrosswordPseudofieldsConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'crossword_pseudofields_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['crossword_pseudofields.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('crossword_pseudofields.settings');

    $tag_options = [
      'h1' => 'h1',
      'h2' => 'h2',
      'h3' => 'h3',
      'h4' => 'h4',
      'p' => 'p',
      'div' => 'div',
      'span' => 'span',
    ];

    $form['redacted'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Redact Solution'),
      '#default_value' => $config->get('redacted'),
      '#description' => $this->t('Redact data such that the solution is not directly available to the front end.'),
    ];
    $form['redacted_warning'] = [
      '#type' => 'container',
      '#markup' => $this->t('If the solution is redacted, it is advised that you hide the Solution button, the Cheat button, and the Show Errors checkbox. These features will not work while the solution is redacted.'),
      '#attributes' => [
        'class' => ['messages messages--warning'],
      ],
      '#states' => [
        'visible' => [
          ":input[name='redacted']" => ['checked' => TRUE],
        ],
      ],
    ];

    $form['congrats'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Congratulatory Message'),
      '#default_value' => $config->get('congrats'),
      '#description' => $this->t('Plaintext message to display when the puzzle is solved successfully.'),
    ];

    $form['details'] = [
      '#type' => 'fieldset',
      '#title' => 'Crossword Details',
      '#tree' => TRUE,
    ];
    $form['details']['title_tag'] = [
      '#type' => 'select',
      '#title' => 'Title',
      '#default_value' => $config->get('details')['title_tag'] ?? '',
      '#options' => $tag_options,
      '#empty_option' => $this->t("Do not render the title"),
      '#prefix' => '<p>Select the html tag to use for rendering these details.</p>',
    ];
    $form['details']['author_tag'] = [
      '#type' => 'select',
      '#title' => 'Author',
      '#default_value' => $config->get('details')['author_tag'] ?? '',
      '#options' => $tag_options,
      '#empty_option' => $this->t("Do not render the author"),
    ];
    $form['details']['notepad_tag'] = [
      '#type' => 'select',
      '#title' => 'Notepad',
      '#default_value' => $config->get('details')['notepad_tag'] ?? '',
      '#options' => $tag_options,
      '#empty_option' => $this->t("Do not render the notepad"),
    ];

    $buttons = [
      'cheat' => 'Cheat',
      'solution' => 'Solution',
      'clear' => 'Clear',
      'undo' => 'Undo',
      'redo' => 'Redo',
      'instructions' => 'Instructions',
    ];
    $form['buttons'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Buttons'),
      '#tree' => TRUE,
    ];
    $form['buttons']['class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CSS Classes'),
      '#default_value' => $config->get('buttons')['class'] ?? '',
      '#description' => $this->t('Enter space-separated CSS classes to be added to the buttons.'),
    ];
    foreach ($buttons as $key => $label) {
      $form['buttons']['buttons'][$key] = [
        '#type' => 'fieldset',
        '#title' => $this->t($label),
        '#tree' => TRUE,
        'show' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Display Button'),
          '#default_value' => $config->get('buttons')['buttons'][$key]['show'] ?? 0,
        ],
        'input_label' => [
          '#type' => 'textfield',
          '#title' => $this->t('Button Text'),
          '#default_value' => $config->get('buttons')['buttons'][$key]['input_label'] ?? '',
          '#states' => [
            'visible' => [
              ":input[name='buttons[buttons][$key][show]']" => ['checked' => TRUE],
            ],
          ],
        ],
        'confirm' => [
          '#type' => 'textfield',
          '#title' => $this->t('Confirmation Message'),
          '#default_value' => $config->get('buttons')['buttons'][$key]['confirm'],
          '#description' => $this->t('If empty, confirmation will not be required.'),
          '#states' => [
            'visible' => [
              ":input[name='buttons[buttons][$key][show]']" => ['checked' => TRUE],
            ],
          ],
        ],
      ];
    }
    $form['clues'] = [
      '#type' => 'fieldset',
      '#title' => 'Show Clues checkbox',
      '#tree' => TRUE,
      'show' => [
        '#type' => 'checkbox',
        '#title' => 'Display',
        '#default_value' => $config->get('clues')['show'] ?? 0,
      ],
      'checked' => [
        '#type' => 'checkbox',
        '#title' => 'Check by default',
        '#default_value' => $config->get('clues')['checked'] ?? 0,
        '#states' => [
          'visible' => [
            ":input[name='clues[show]']" => ['checked' => TRUE],
          ],
        ],
      ],
      'input_label' => [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#default_value' => $config->get('clues')['input_label'] ?? '',
        '#states' => [
          'visible' => [
            ":input[name='clues[show]']" => ['checked' => TRUE],
          ],
        ],
      ],
    ];
    $form['errors'] = [
      '#type' => 'fieldset',
      '#title' => 'Show Errors checkbox',
      '#tree' => TRUE,
      'show' => [
        '#type' => 'checkbox',
        '#title' => 'Display',
        '#default_value' => $config->get('errors')['show'] ?? 0,
      ],
      'checked' => [
        '#type' => 'checkbox',
        '#title' => 'Check by default',
        '#default_value' => $config->get('errors')['checked'] ?? 0,
        '#states' => [
          'visible' => [
            ":input[name='errors[show]']" => ['checked' => TRUE],
          ],
        ],
      ],
      'input_label' => [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#default_value' => $config->get('errors')['input_label'] ?? '',
        '#states' => [
          'visible' => [
            ":input[name='errors[show]']" => ['checked' => TRUE],
          ],
        ],
      ],
    ];
    $form['references'] = [
      '#type' => 'fieldset',
      '#title' => 'Show References checkbox',
      '#tree' => TRUE,
      'show' => [
        '#type' => 'checkbox',
        '#title' => 'Display',
        '#default_value' => $config->get('references')['show'] ?? 0,
      ],
      'checked' => [
        '#type' => 'checkbox',
        '#title' => 'Check by default',
        '#default_value' => $config->get('references')['checked'] ?? 0,
        '#states' => [
          'visible' => [
            ":input[name='references[show]']" => ['checked' => TRUE],
          ],
        ],
      ],
      'input_label' => [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#default_value' => $config->get('references')['input_label'] ?? '',
        '#states' => [
          'visible' => [
            ":input[name='references[show]']" => ['checked' => TRUE],
          ],
        ],
      ],
    ];
    $form['rebus'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Rebus Entry checkbox'),
      '#tree' => TRUE,
      'show' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Always Display'),
        '#default_value' => $config->get('rebus')['show'] ?? 0,
        '#description' => $this->t('If checked, the rebus entry button will always appear with the puzzle. If un-checked, the rebus entry checkbox will only be displayed on rebus puzzles.'),
      ],
      'input_label' => [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#default_value' => $config->get('rebus')['input_label'] ?? '',
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('crossword_pseudofields.settings')
      ->set('redacted', $form_state->getValue('redacted'))
      ->set('congrats', $form_state->getValue('congrats'))
      ->set('details', $form_state->getValue('details'))
      ->set('buttons', $form_state->getValue('buttons'))
      ->set('clues', $form_state->getValue('clues'))
      ->set('errors', $form_state->getValue('errors'))
      ->set('references', $form_state->getValue('references'))
      ->set('rebus', $form_state->getValue('rebus'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
