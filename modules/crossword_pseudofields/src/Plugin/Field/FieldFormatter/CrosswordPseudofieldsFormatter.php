<?php

namespace Drupal\crossword_pseudofields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\crossword\Plugin\Field\FieldFormatter\CrosswordFormatter;

/**
 * Plugin implementation of the 'crossword_pseudofields' formatter.
 *
 * @FieldFormatter(
 *   id = "crossword_pseudofields",
 *   label = @Translation("Crossword Puzzle (pseudofields)"),
 *   weight = "7",
 *   field_types = {
 *     "crossword"
 *   }
 * )
 */
class CrosswordPseudofieldsFormatter extends CrosswordFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options = parent::defaultSettings();
    unset($options['print']);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['warning'] = [
      '#type' => 'container',
      '#markup' => $this->t("You probably don't need to use this Field Formatter. It exists primarily for technical reasons. Consider using the pseudofields directly. <a href='/admin/config/content/crossword-pseudofields'>Configure pseudfield details here.</a>"),
      '#attributes' => [
        'class' => ['messages messages--warning'],
      ],
      '#weight' => -100,
    ];
    unset($form['print']);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $files = $this->getEntitiesToView($items, $langcode);
    if (empty($files)) {
      return [];
    }
    $file = $files[0];

    $data = $this->crosswordDataService->getData($file, $this->getSetting('redacted'));

    $elements = [
      'title' => $this->getTitle($data),
      'author' => $this->getAuthor($data),
      'notepad' => $this->getNotepad($data),
      'controls' => $this->getControls($file),
      'active_clue' => [
        '#theme' => 'crossword_active_clues',
        '#congrats' => $this->getSetting('congrats'),
        '#pseudofield' => TRUE,
        '#field_formatter' => $this->getPluginId(),
      ],
      'grid' => $this->getGrid($file),
      'across' => $this->getClues($data, 'across'),
      'down' => $this->getClues($data, 'down'),
      'playable' => [
        '#attached' => [
          'library' => [
            'crossword/crossword.default',
          ],
          'drupalSettings' => [
            'crossword' => [
              'data' => $data,
              'selector' => 'body',
            ],
          ],
        ],
      ],
      '#attributes' => [
        'class' => [
          $this->getSetting('errors')['show'] && $this->getSetting('errors')['checked'] ? 'show-errors' : '',
          $this->getSetting('references')['show'] && $this->getSetting('references')['checked'] ? 'show-references' : '',
          $this->getSetting('clues')['show'] && !$this->getSetting('clues')['checked'] ? 'hide-clues' : '',
        ],
      ],
      '#cache' => [
        'tags' => $file->getCacheTags(),
      ],
    ];
    // Add grid library.
    $elements['grid']['#attached'] = [
      'library' => 'crossword/crossword.grid',
    ];
    // Add cache/attach info to each pseudofield.
    foreach ($elements as $key => $element) {
      if (substr($key, 0, 1) != '#') {
        $elements[$key]['#cache'] = $elements['#cache'];
      }
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function view(FieldItemListInterface $items, $langcode = NULL) {
    // Default the language to the current content language.
    if (empty($langcode)) {
      $langcode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    }
    $elements = $this->viewElements($items, $langcode);
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $return = [];
    $return[] = $this->t('Click gear to read WARNING!');
    return $return;
  }

}
