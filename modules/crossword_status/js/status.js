(function ($, Drupal, once, drupalSettings) {

  Drupal.behaviors.crosswordStatus = {
    attach: function (context, settings) {
      // Part I: TRACKING //
      // Listen and add status to local storage.
      if (drupalSettings.crossword && drupalSettings.crossword.data && drupalSettings.crossword.data.id) {
        var id = drupalSettings.crossword.data.id;
        once('crossword-status-track', 'body', context).forEach(function(element){
          $(element).on('crossword-solved', function () {
            Drupal.behaviors.crosswordStatus.setStatus(id, 'solved');
          })
            .on('crossword-revealed', function () {
              Drupal.behaviors.crosswordStatus.setStatus(id, 'revealed');
            })
            .on('crossword-clear', function () {
              Drupal.behaviors.crosswordStatus.setStatus(id, '');
              // Add the active trigger back.
              $(this).one('crossword-active', function () {
                if (!Drupal.behaviors.crosswordStatus.getStatus(id)) {
                  Drupal.behaviors.crosswordStatus.setStatus(id, 'in-progress');
                }
              });
            })
            .one('crossword-active', function () {
              // We only let this trigger once since it's so common..
              if (!Drupal.behaviors.crosswordStatus.getStatus(id)) {
                Drupal.behaviors.crosswordStatus.setStatus(id, 'in-progress');
              }
            });
        });
      }
      // Part II: STATUS DISPLAY //
      // Add classes to any crossword fields on the page.
      once('crossword-status-show', '[data-crossword-fid]', context).forEach(function(element){
        var fid = $(element).data('crossword-fid');
        var status = Drupal.behaviors.crosswordStatus.getStatus(fid);
        if (status) {
          $(element).addClass('crossword-status-' + status);
        }
        else {
          $(element).addClass('crossword-status-none');
        }
        if (fid == id) {
          $(element).addClass('crossword-status-current');
        }
      });
    },
    setStatus: function(id, status) {
      if ($status_json = localStorage.getItem('crossword-status')) {
        var status_object = JSON.parse($status_json);
      }
      else {
        var status_object = {};
      }
      status_object[id] = status;
      var status_json = JSON.stringify(status_object);
      localStorage.setItem('crossword-status', status_json);
    },
    getStatus: function(id) {
      if (status_json = localStorage.getItem('crossword-status')) {
        var status_object = JSON.parse(status_json);
        return status_object[id];
      }
    }
  }
})(jQuery, Drupal, once, drupalSettings);
