CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

 This module provides a simple client-side tracking system. Using local storage
 puzzles are tracked as being in-progress, solved, and revealed. This is purely
 client-side. A class is added after page load to crossword fields that can
 be used in styling, etc. The classes are:
 - crossword-status-none
 - crossword-status-in-progress
 - crossword-status-solved
 - crossword-status-revealed
 - crossword-status-current

 These classes are added to elements that have a data-crossword-fid attribute.
 The fid of the crossword file is used as the unique id for all of this.

REQUIREMENTS
------------

 It requires its parent module (crossword).

RECOMMENDED MODULES
-------------------

 Nope.

INSTALLATION
------------

 Install as usual, see https://www.drupal.org/node/1897420 for further
 information.

CONFIGURATION
-------------

 None.

TROUBLESHOOTING
---------------

 This module does not do anything visual. That is all up to you.

 The attribute and library are added using a preprocess field function.
 If your field template doesn't include attributes then this won't work
 without some extra work on your part.

 Similarly, if rendering Fields in a View, make sure to check the
 "Use field template" option for any Crossword field.

MAINTAINERS
-----------

 danflanagan8 (crossword enthusiast)
