CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

 This module allows the colors to be customized for the playabe crossword
 puzzle to be customized through a configuration form. This is an easy
 alternative to overriding the default colors in a custom theme.

REQUIREMENTS
------------

 It requires its parent module (crossword).

RECOMMENDED MODULES
-------------------

 Nope.

INSTALLATION
------------

 Install as usual, see https://www.drupal.org/node/1897420 for further
 information.

CONFIGURATION
-------------

 There's a form at /admin/config/crossword/colors.

TROUBLESHOOTING
---------------

 After updating the colors, you probably need to clear cache before the
 updated stylesheets will be pulled in.

MAINTAINERS
-----------

 danflanagan8 (crossword enthusiast)
