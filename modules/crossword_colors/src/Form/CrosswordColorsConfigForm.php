<?php

namespace Drupal\crossword_colors\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure crossword_colors settings for this site.
 */
class CrosswordColorsConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'crossword_colors_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['crossword_colors.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('crossword_colors.settings');

    $form['active_highlight'] = [
      '#type' => 'color',
      '#title' => $this->t('Active Highlight Color'),
      '#description' => $this->t('Background color for squares within the active clue and for the active clue in the clues list.'),
      '#default_value' => $config->get('active_highlight'),
    ];
    $form['active_square'] = [
      '#type' => 'color',
      '#title' => $this->t('Active Square Color'),
      '#description' => $this->t('Background color for the one active/focused square.'),
      '#default_value' => $config->get('active_square'),
    ];
    $form['reference_highlight'] = [
      '#type' => 'color',
      '#title' => $this->t('Reference Highlight Color'),
      '#description' => $this->t('Background color for squares within referenced clues and for referenced clues in the clues list.'),
      '#default_value' => $config->get('reference_highlight'),
    ];
    $form['reference_text'] = [
      '#type' => 'color',
      '#title' => $this->t('Reference Text Color'),
      '#description' => $this->t('Text color for referenced clue text displayed in the active clue header.'),
      '#default_value' => $config->get('reference_text'),
    ];
    $form['error_text'] = [
      '#type' => 'color',
      '#title' => $this->t('Error Text Color'),
      '#description' => $this->t('Text color for errant squares and for text errant clues in the clues list .'),
      '#default_value' => $config->get('error_text'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('crossword_colors.settings')
      ->set('active_highlight', $form_state->getValue('active_highlight'))
      ->set('active_square', $form_state->getValue('active_square'))
      ->set('reference_highlight', $form_state->getValue('reference_highlight'))
      ->set('reference_text', $form_state->getValue('reference_text'))
      ->set('error_text', $form_state->getValue('error_text'))
      ->save();
    \Drupal::service('crossword_colors.service')->saveCrosswordColorsCss();
    // Clear caches needed to see new colors immediately.
    // Relatively lightweight compared to drupal_flush_all_caches().
    \Drupal::service('library.discovery')->clearCachedDefinitions();
    \Drupal::service('asset.css.collection_optimizer')->deleteAll();
    \Drupal::service('asset.query_string')->reset();
    parent::submitForm($form, $form_state);
  }

}
