<?php

namespace Drupal\crossword_colors;

use Drupal\Component\Utility\Color as ColorUtility;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\File\FileSystem;

/**
 * CrosswordColorsService handles the crossword_colors.css file.
 */
class CrosswordColorsService {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * Colors config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The URI where the css should be saved.
   */
  const URI = 'public://crossword-colors.css';

  /**
   * Construct the Crossword Image Service.
   *
   * @param \Drupal\Core\File\FileSystem $file_system
   *   The file system.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Config factory.
   */
  public function __construct(FileSystem $file_system, ConfigFactory $config_factory) {
    $this->fileSystem = $file_system;
    $this->config = $config_factory->get('crossword_colors.settings');
  }

  /**
   * Gets the uri of css file, creating one if necessary.
   */
  public function getCrosswordColorsCssUri() {
    if (file_exists(self::URI)) {
      return self::URI;
    }
    else {
      return $this->saveCrosswordColorsCss();
    }
  }

  /**
   * Loads config and uses it to save a new css file.
   */
  public function saveCrosswordColorsCss() {
    $data = '';
    // Reference highlight is before active highlight so that active highlight
    // takes precedence if an active clue crosses a referenced clue.
    $reference_highlight = $this->config->get('reference_highlight');
    if ($reference_highlight && ColorUtility::validateHex($reference_highlight)) {
      $data .= "show-references .crossword-square.reference, .show-references .crossword-clue.reference {background-color: $reference_highlight;}";
    }
    $active_hightlight = $this->config->get('active_highlight');
    if ($active_hightlight && ColorUtility::validateHex($active_hightlight)) {
      $data .= ".crossword-square.highlight, .crossword-clue.active {background-color: $active_hightlight;}";
    }
    $active_square = $this->config->get('active_square');
    if ($active_square && ColorUtility::validateHex($active_square)) {
      $data .= ".crossword-square.active {background-color: $active_square;}";
    }
    $error_text = $this->config->get('error_text');
    if ($error_text && ColorUtility::validateHex($error_text)) {
      $data .= ".show-errors .crossword-square.error .square-fill, .show-errors .crossword-clue.error {color: $error_text;}";
    }
    $reference_text = $this->config->get('reference_text');
    if ($reference_text && ColorUtility::validateHex($reference_text)) {
      $data .= ".show-references .active-clues .reference {color: $reference_text;}";
    }
    return $this->fileSystem->saveData($data, self::URI, FileSystem::EXISTS_REPLACE);
  }

}
