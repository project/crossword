<?php

namespace Drupal\crossword_media\Plugin\media\Source;

use Drupal\Core\Form\FormStateInterface;
use Drupal\crossword_image\CrosswordImageServiceInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\media\Plugin\media\Source\File;
use Drupal\file\FileInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\crossword_image\CrosswordImageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Crossword media source.
 *
 * @MediaSource(
 *   id = "crossword",
 *   label = @Translation("Crossword"),
 *   description = @Translation("Use local crossword files for reusable media."),
 *   default_thumbnail_filename = "generic.png",
 *   allowed_field_types = {"crossword"},
 * )
 */
class Crossword extends File implements ContainerFactoryPluginInterface {

  /**
   * The crossword image manager.
   *
   * @var \Drupal\crossword_image\CrosswordImageManager
   */
  protected $crosswordImageManager;

  /**
   * The crossword image service.
   *
   * @var \Drupal\crossword_image\CrosswordImageServiceInterface
   */
  protected $crosswordImageService;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\crossword_image\CrosswordImageManager $crossword_image_manager
   *   The crossword image manager service.
   * @param \Drupal\crossword_image\CrosswordImageServiceInterface $crossword_image_service
   *   The crossword image service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, FieldTypePluginManagerInterface $field_type_manager, ConfigFactoryInterface $config_factory, CrosswordImageManager $crossword_image_manager, CrosswordImageServiceInterface $crossword_image_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $field_type_manager, $config_factory);
    $this->crosswordImageManager = $crossword_image_manager;
    $this->crosswordImageService = $crossword_image_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('config.factory'),
      $container->get('crossword.manager.image'),
      $container->get('crossword.image_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    return parent::createSourceField($type)->set('settings', ['file_extensions' => 'txt puz']);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'crossword_image_plugin' => 'thumbnail',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['crossword_image_plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Crossword Image Plugin for Thumbnail'),
      '#default_value' => $this->configuration['crossword_image_plugin'],
      '#options' => $this->crosswordImageManager->getCrosswordImageOptionList(),
      '#required' => TRUE,
      '#description' => $this->t('Select the Crossword Image Plugin that should be used when creating the thumbnail for this Media.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getThumbnail(FileInterface $file) {
    return $this->crosswordImageService->getImageUri($file, $this->configuration['crossword_image_plugin']);
  }

}
