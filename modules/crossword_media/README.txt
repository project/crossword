CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

 This module provides a simple extension of the File media source that allows
 the crossword field type to be used as a media source.

 The Crossword Image Plugin system provided by the crossword_image module
 (which is included in the crossword module) is used for the generation
 of the media thumbnail. See the crossword_image module for details on
 the plugins that are provided.

REQUIREMENTS
------------

 It requires its parent module (crossword) and the crossword_image sub-module.
 And of course core media!

RECOMMENDED MODULES
-------------------

 The crossword_pseudofields sub-module adds pseudofields to Crossword media.

INSTALLATION
------------

 Install as usual, see https://www.drupal.org/node/1897420 for further
 information.

CONFIGURATION
-------------

 When creating a Media Type with Crossword as the source type, you get to select
 which crossword_image plugin should be used for generating the thumbnail.

TROUBLESHOOTING
---------------

 No known common issues.

MAINTAINERS
-----------

 danflanagan8 (crossword enthusiast)
