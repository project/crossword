CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

 This is a sub-module of crossword. This module is intended to house
 features related to making a crossword puzzle easy to download.

REQUIREMENTS
------------

 This module requires contrib projects token and file_download_link.
 It also requires its parent module (crossword) as well as the crossword_image
 sub-module.

RECOMMENDED MODULES
-------------------

 The contrib module entity_print may be useful for creating pdfs out
 of a puzzle. Note however that the Crossword Download Module does not include
 any support for pdfs.

INSTALLATION
------------

 Install as usual, see https://www.drupal.org/node/1897420 for further
 information.

CONFIGURATION
-------------

 This module provides the File Download Link and Crossword Image (download link)
 field formatters for Crossword fields. See the file_download_link project
 page for additional information related to these formatters.

 https://www.drupal.org/project/file_download_link

 A recommended use case would be to render a Crossword Field using the
 Crossword Image (download link) field formatter configured to use the
 solution_thumbnail. This would provide a link to download the solution
 as an image.

TROUBLESHOOTING
---------------

 No known common issues.

MAINTAINERS
-----------

 danflanagan8 (crossword enthusiast)
