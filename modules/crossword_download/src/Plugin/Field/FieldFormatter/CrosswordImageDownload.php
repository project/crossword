<?php

namespace Drupal\crossword_download\Plugin\Field\FieldFormatter;

use Drupal\file_download_link\Plugin\Field\FieldFormatter\FileDownloadLink;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'crossword_image_download' formatter.
 *
 * @FieldFormatter(
 *   id = "crossword_image_download",
 *   label = @Translation("Crossword Image (download link)"),
 *   weight = "5",
 *   field_types = {
 *     "crossword"
 *   },
 * )
 */
class CrosswordImageDownload extends FileDownloadLink {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return ['crossword_image' => 'solution_thumbnail'] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['crossword_image'] = [
      '#type' => 'select',
      '#options' => \Drupal::service('crossword.manager.image')->getCrosswordImageOptionList(),
      '#default_value' => $this->getSetting('crossword_image'),
      '#title' => $this->t('Crossword Image Type'),
      '#description' => $this->t('Choose the Crossword Image Plugin to be used when generating an image from the Crossword file.'),
      '#weight' => -100,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $image_type = $this->t('Image Type: @crossword_image', ['@crossword_image' => $this->getSetting('crossword_image')]);
    array_unshift($summary, $image_type);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntitiesToView(EntityReferenceFieldItemListInterface $items, $langcode) {
    $files = parent::getEntitiesToView($items, $langcode);
    $images = [];
    $crossword_image_service = \Drupal::service('crossword.image_service');
    foreach ($files as $delta => $file) {
      $image = $crossword_image_service->getImageEntity($file, $this->getSetting('crossword_image'));
      if (!empty($image)) {
        $image->addCacheableDependency($file);
        $images[$delta] = $image;
      }
    }

    return $images;
  }

  /**
   * {@inheritdoc}
   */
  protected function getExampleToken() {
    $entity_type = $this->fieldDefinition->getTargetEntityTypeId();
    $field = $this->fieldDefinition->getName();
    $entity_type = $this->tokenEntityMapper->getTokenTypeForEntityType($entity_type);
    return "Solution for [$entity_type:$field:entity:crossword_title] ([file:extension], [file:size])";
  }

  /**
   * {@inheritdoc}
   */
  protected function getTokenExampleMarkup() {
    return $this->t('<p>The "file" token represents the image. You can still access the Crossword file with other tokens. Imagine the image is a solution thumbnail such that the user will be download the solution. You want to show the title of the Crossword along with information about the type and size of the image. You could do it like this:<code>@example</code></p>', ['@example' => $this->getExampleToken()]);
  }

}
