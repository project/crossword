<?php

namespace Drupal\crossword_download\Plugin\Field\FieldFormatter;

use Drupal\file_download_link\Plugin\Field\FieldFormatter\FileDownloadLink;

/**
 * Plugin implementation of the 'crossword_file_download_link' formatter.
 *
 * @FieldFormatter(
 *   id = "crossword_file_download_link",
 *   label = @Translation("File Download Link"),
 *   weight = "6",
 *   field_types = {
 *     "crossword",
 *   }
 * )
 */
class CrosswordFileDownloadLink extends FileDownloadLink {

  /**
   * {@inheritdoc}
   */
  protected function getExampleToken() {
    return "[file:crossword_title] ([file:crossword_dimensions])";
  }

  /**
   * {@inheritdoc}
   */
  protected function getTokenExampleMarkup() {
    return $this->t('<p>You can show the Crossword title followed by the Crossword dimensions like this:<code>@example</code></p>', ['@example' => $this->getExampleToken()]);
  }

}
