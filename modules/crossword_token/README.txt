CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

 This module provides tokens for Crossword Files like...
  - crossword_title
  - crossword_author
  - crossword_dimensions
  - crossword_dimension_across
  - crossword_dimension_down

  as well as tokens for crossword_image.

REQUIREMENTS
------------

 It requires its parent module (crossword) and the contrib module token.

RECOMMENDED MODULES
-------------------

 The world is your oyster.

INSTALLATION
------------

 Install as usual, see https://www.drupal.org/node/1897420 for further
 information.

CONFIGURATION
-------------

 None.

TROUBLESHOOTING
---------------

 The Crossword tokens are technically file tokens. They will appear as options
 for files that aren't crosswords. Be smart. Only use them on crosswords.

MAINTAINERS
-----------

 danflanagan8 (crossword enthusiast)
