<?php

/**
 * @file
 * Token callbacks for the crossword module.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\image\Entity\ImageStyle;

/**
 * Implements hook_token_info().
 */
function crossword_token_token_info() {
  $info['tokens']['file']['crossword_author'] = [
    'name' => t('Crossword Author'),
    'description' => t('Author of the Crossword'),
  ];
  $info['tokens']['file']['crossword_title'] = [
    'name' => t('Crossword Title'),
    'description' => t('Title of the Crossword'),
  ];
  $info['tokens']['file']['crossword_dimensions'] = [
    'name' => t('Crossword Dimensions'),
    'description' => t('Dimensions of the Crossword'),
  ];
  $info['tokens']['file']['crossword_dimension_across'] = [
    'name' => t('Crossword Dimension Across'),
    'description' => t('Dimension of the Crossword'),
  ];
  $info['tokens']['file']['crossword_dimension_down'] = [
    'name' => t('Crossword Dimension Down'),
    'description' => t('Dimension of the Crossword'),
  ];

  // Add image tokens if crossword_image is enabled.
  if (\Drupal::moduleHandler()->moduleExists('crossword_image')) {
    $options = \Drupal::service('crossword.manager.image')->getCrosswordImageOptionList();
    $plugins_string = implode(", ", array_keys($options));
    foreach ($options as $id => $label) {
      $info['tokens']['file']['crossword_image'] = [
        'name' => 'Crossword Image',
        'description' => "Crossword Image Url. Replace ? with a crossword_image plugin id. Possible values are $plugins_string. You may also apply an image style by adding another colon (:) followed by an image style machine name.",
        'dynamic' => TRUE,
      ];
    }
  }

  return $info;
}

/**
 * Implements hook_tokens().
 */
function crossword_token_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  // Crossword tokens, which are special file tokens.
  if ($type == 'file' && !empty($data['file'])) {
    $file = $data['file'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'crossword_author':
          $data_service = \Drupal::service('crossword.data_service');
          $author = $data_service->getAuthor($file);
          if ($author) {
            $replacements[$original] = $author;
          }
          break;

        case 'crossword_title':
          $data_service = \Drupal::service('crossword.data_service');
          $title = $data_service->getTitle($file);
          if ($title) {
            $replacements[$original] = $title;
          }
          break;

        case 'crossword_dimensions':
          $data_service = \Drupal::service('crossword.data_service');
          $dimensions = $data_service->getDimensions($file);
          if ($dimensions) {
            $replacements[$original] = $dimensions;
          }
          break;

        case 'crossword_dimension_down':
          $data_service = \Drupal::service('crossword.data_service');
          $dimension = $data_service->getDimensionDown($file);
          if ($dimension) {
            $replacements[$original] = $dimension;
          }
          break;

        case 'crossword_dimension_across':
          $data_service = \Drupal::service('crossword.data_service');
          $dimension = $data_service->getDimensionAcross($file);
          if ($dimension) {
            $replacements[$original] = $dimension;
          }
          break;

      }
    }

    // Handle crossword_image tokens.
    // Dynamic, modeled off of random:hash token.
    // [file:crossword_image:{crossword_image_plugin_id}:{image_style}].
    if (\Drupal::moduleHandler()->moduleExists('crossword_image')) {
      if ($crossword_image_tokens = \Drupal::token()->findWithPrefix($tokens, 'crossword_image')) {
        $options = \Drupal::service('crossword.manager.image')->getCrosswordImageOptionList();
        foreach ($crossword_image_tokens as $name => $original) {
          $parts = explode(":", $name);
          if (count($parts) > 2) {
            break;
          }
          $image_plugin_id = $parts[0] ?? NULL;
          $image_style = $parts[1] ?? NULL;
          if (isset($options[$image_plugin_id])) {
            $image = \Drupal::service('crossword.image_service')->getImageEntity($file, $image_plugin_id);
            if ($image) {
              if ($image_style && $style = ImageStyle::load($image_style)) {
                $replacements[$original] = $style->buildUrl($image->getFileUri());
              }
              elseif (!isset($parts[1])) {
                $replacements[$original] = \Drupal::service('file_url_generator')->generateAbsoluteString($image->getFileUri());
              }
            }
          }
        }
      }
    }
  }

  return $replacements;
}
