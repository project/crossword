<?php

namespace Drupal\crossword_image;

use Drupal\file\FileInterface;

/**
 * Interface for CrosswordImageService.
 */
interface CrosswordImageServiceInterface {

  /**
   * Get a uri to the image.
   *
   * @param \Drupal\file\FileInterface $file
   *   The Crossword file.
   * @param string $plugin_id
   *   Id of the crossword_image plugin to use.
   *
   * @return string|null
   *   The uri or null.
   */
  public function getImageUri(FileInterface $file, string $plugin_id);

  /**
   * Get a file entity of the image.
   *
   * @param \Drupal\file\FileInterface $file
   *   The Crossword file.
   * @param string $plugin_id
   *   Id of the crossword_image plugin to use.
   *
   * @return \Drupal\file\FileInterface|null
   *   The file entity of the image or null.
   */
  public function getImageEntity(FileInterface $file, string $plugin_id);

  /**
   * Regenerate existing crossword images.
   */
  public function regenerateCrosswordImages(array $crossword_image_plugin_ids = []);

}
