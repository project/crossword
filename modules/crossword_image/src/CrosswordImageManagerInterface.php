<?php

namespace Drupal\crossword_image;

/**
 * Interface for Crossword Image Manager.
 */
interface CrosswordImageManagerInterface {

  /**
   * Get an options list suitable for form elements for image selection.
   *
   * @return array
   *   An array of options keyed by plugin ID with label values.
   */
  public function getCrosswordImageOptionList();

}
