<?php

namespace Drupal\crossword_image;

use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\crossword\CrosswordDataServiceInterface;
use Drupal\Core\ImageToolkit\ImageToolkitManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A base class for plugins that create images from crossword files.
 */
abstract class CrosswordImagePluginBase extends PluginBase implements CrosswordImagePluginInterface, ContainerFactoryPluginInterface {

  /**
   * The crossword data service.
   *
   * @var \Drupal\crossword\CrosswordDataServiceInterface
   */
  protected $crosswordDataService;

  /**
   * The image toolkit plugin manage.
   *
   * @var \Drupal\Core\ImageToolkit\ImageToolkitManager
   */
  protected $toolkitManager;

  /**
   * The extension list.
   *
   * @var \Drupal\Core\Extension\ExtensionList
   */
  protected $extensionList;

  /**
   * Construct the Crossword Image Plugin.
   */
  public function __construct($configuration, $plugin_id, array $plugin_definition, CrosswordDataServiceInterface $crossword_data_service, ImageToolkitManager $toolkit_manager, ExtensionList $extension_list) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->crosswordDataService = $crossword_data_service;
    $this->toolkitManager = $toolkit_manager;
    $this->extensionList = $extension_list;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('crossword.data_service'),
      $container->get('image.toolkit.manager'),
      $container->get('extension.list.module')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->getPluginDefinition()['type'];
  }

  /**
   * {@inheritdoc}
   */
  public function getToolkit() {
    return $this->toolkitManager->createInstance($this->getPluginDefinition()['toolkit']);
  }

}
