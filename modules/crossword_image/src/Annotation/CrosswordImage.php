<?php

namespace Drupal\crossword_image\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a CrosswordImage item annotation object.
 *
 * @Annotation
 */
class CrosswordImage extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The title of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The php image type (e.g. IMAGETYPE_PNG).
   *
   * @var int
   */
  public $type;

  /**
   * Toolkit id. Probably "gd".
   *
   * @var string
   */
  public $toolkit;

}
