<?php

namespace Drupal\crossword_image;

use Drupal\file\FileInterface;

/**
 * Interface for plugins that create images from crossword files.
 */
interface CrosswordImagePluginInterface {

  /**
   * Returns an image resource representing a preview of the puzzle.
   *
   * @param \Drupal\file\FileInterface $file
   *   The crossword file.
   *
   * @return resource
   *   An image resource to be used as a thumbnail.
   */
  public function createImageResource(FileInterface $file);

  /**
   * The type of image to be saved, as given in annotation.
   *
   * @return string
   *   The php image type to be saved.
   */
  public function getType();

  /**
   * The toolkit to be used.
   *
   * @return \Drupal\Core\ImageToolkit\ImageToolkitInterface
   *   The php image type to be saved.
   */
  public function getToolkit();

}
