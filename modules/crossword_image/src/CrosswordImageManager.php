<?php

namespace Drupal\crossword_image;

use Drupal\Component\Plugin\Mapper\MapperInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Gathers the crossword file parser plugins.
 */
class CrosswordImageManager extends DefaultPluginManager implements CrosswordImageManagerInterface, MapperInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/crossword/crossword_image', $namespaces, $module_handler, 'Drupal\crossword_image\CrosswordImagePluginInterface', 'Drupal\crossword_image\Annotation\CrosswordImage');
  }

  /**
   * {@inheritdoc}
   */
  public function getCrosswordImageOptionList() {
    $options = [];
    foreach ($this->getDefinitions() as $definition) {
      $options[$definition['id']] = $definition['title'];
    }
    return $options;
  }

}
