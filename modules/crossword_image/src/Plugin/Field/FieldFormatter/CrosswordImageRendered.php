<?php

namespace Drupal\crossword_image\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\crossword_image\CrosswordImageServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\Cache;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\crossword_image\CrosswordImageManager;
use Drupal\crossword\CrosswordDataServiceInterface;

/**
 * Plugin implementation of the 'crossword_image_rendered' formatter.
 *
 * @FieldFormatter(
 *   id = "crossword_image_rendered",
 *   label = @Translation("Crossword Image"),
 *   weight = "4",
 *   field_types = {
 *     "crossword"
 *   },
 * )
 */
class CrosswordImageRendered extends ImageFormatter implements ContainerFactoryPluginInterface {

  /**
   * The crossword image manager service.
   *
   * @var \Drupal\crossword_image\CrosswordImageManager
   */
  protected $crosswordImageManager;

  /**
   * The crossword data service.
   *
   * @var \Drupal\crossword\CrosswordDataServiceInterface
   */
  protected $crosswordDataService;

  /**
   * The crossword image service.
   *
   * @var \Drupal\crossword_image\CrosswordImageServiceInterface
   */
  protected $crosswordImageService;

  /**
   * Constructs an ImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   *   The image style storage.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   * @param \Drupal\crossword_image\CrosswordImageManager $crossword_image_manager
   *   The crossword image manager service.
   * @param \Drupal\crossword\CrosswordDataServiceInterface $crossword_data_service
   *   The crossword data service.
   * @param \Drupal\crossword_image\CrosswordImageServiceInterface $crossword_image_service
   *   The crossword image service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user, EntityStorageInterface $image_style_storage, FileUrlGeneratorInterface $file_url_generator, CrosswordImageManager $crossword_image_manager, CrosswordDataServiceInterface $crossword_data_service, CrosswordImageServiceInterface $crossword_image_service) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $current_user, $image_style_storage, $file_url_generator);
    $this->crosswordImageManager = $crossword_image_manager;
    $this->crosswordDataService = $crossword_data_service;
    $this->crosswordImageService = $crossword_image_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('image_style'),
      $container->get('file_url_generator'),
      $container->get('crossword.manager.image'),
      $container->get('crossword.data_service'),
      $container->get('crossword.image_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return ['crossword_image' => 'thumbnail'] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['crossword_image'] = [
      '#type' => 'select',
      '#options' => $this->crosswordImageManager->getCrosswordImageOptionList(),
      '#default_value' => $this->getSetting('crossword_image'),
      '#title' => $this->t('Crossword Image Type'),
      '#description' => $this->t('Choose the Crossword Image Plugin to be used when generating an image from the Crossword file.'),
      '#weight' => -100,
    ];
    $link_types = [
      'content' => $this->t('Content'),
      'image' => $this->t('Image'),
      'file' => $this->t('Crossword File'),
    ];
    $form['image_link']['#options'] = $link_types;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $image_styles = image_style_options(FALSE);
    // Unset possible 'No defined styles' option.
    unset($image_styles['']);
    // Styles could be lost because of enabled/disabled modules that defines
    // their styles in code.
    $image_style_setting = $this->getSetting('image_style');
    if (isset($image_styles[$image_style_setting])) {
      $summary[] = $this->t('Image style: @style', ['@style' => $image_styles[$image_style_setting]]);
    }
    else {
      $summary[] = $this->t('Original image');
    }

    $link_types = [
      'content' => $this->t('Link to content'),
      'image' => $this->t('Link to image'),
      'file' => $this->t('Link to crossword file'),
    ];
    // Display this setting only if image is linked.
    $image_link_setting = $this->getSetting('image_link');
    if (isset($link_types[$image_link_setting])) {
      $summary[] = $link_types[$image_link_setting];
    }
    $image_type = $this->t('Image Type: @crossword_image', ['@crossword_image' => $this->getSetting('crossword_image')]);
    array_unshift($summary, $image_type);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $files = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($files)) {
      return $elements;
    }

    $url = NULL;
    $image_link_setting = $this->getSetting('image_link');
    // Check if the formatter involves a link.
    if ($image_link_setting == 'content') {
      $entity = $items->getEntity();
      if (!$entity->isNew()) {
        $url = $entity->toUrl();
      }
    }
    elseif ($image_link_setting == 'file') {
      $link_file = TRUE;
    }
    elseif ($image_link_setting == 'image') {
      $link_image = TRUE;
    }

    $image_style_setting = $this->getSetting('image_style');

    // Collect cache tags to be added for each item in the field.
    $base_cache_tags = [];
    if (!empty($image_style_setting)) {
      $image_style = $this->imageStyleStorage->load($image_style_setting);
      $base_cache_tags = $image_style->getCacheTags();
    }

    foreach ($files as $delta => $file) {
      $crossword_data = $this->crosswordDataService->getData($file);
      $image = $this->crosswordImageService->getImageEntity($file, $this->getSetting('crossword_image'));
      if (empty($image)) {
        continue;
      }
      $image_uri = $image->getFileUri();
      $cache_contexts = [];
      if (isset($link_image)) {

        // @todo Wrap in file_url_transform_relative(). This is currently
        // impossible. As a work-around, we currently add the 'url.site' cache
        // context to ensure different file URLs are generated for different
        // sites in a multisite setup, including HTTP and HTTPS versions of the
        // same site. Fix in https://www.drupal.org/node/2646744.
        $url = $this->fileUrlGenerator->generate($image_uri);
        $cache_contexts[] = 'url.site';
      }
      elseif (isset($link_file)) {
        $url = $this->fileUrlGenerator->generate($file->getFileUri());
        $cache_contexts[] = 'url.site';
      }
      $cache_tags = Cache::mergeTags($base_cache_tags, $file->getCacheTags(), $image->getCacheTags());

      // Extract field item attributes for the theme function, and unset them
      // from the $item so that the field template does not re-render them.
      $item = $file->_referringItem;
      $item_attributes = $item->_attributes;
      unset($item->_attributes);

      $item->uri = $image_uri;
      $item->alt = $crossword_data['title'];
      $elements[$delta] = [
        '#theme' => 'image_formatter',
        '#item' => $item,
        '#item_attributes' => $item_attributes,
        '#image_style' => $image_style_setting,
        '#url' => $url,
        '#cache' => [
          'tags' => $cache_tags,
          'contexts' => $cache_contexts,
        ],
      ];
    }

    return $elements;
  }

}
