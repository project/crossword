<?php

namespace Drupal\crossword_image\Plugin\crossword\crossword_image;

use Drupal\file\FileInterface;

/**
 * This makes an image of the blank grid with numbers and hints.
 *
 * It's a little bigger in pixels and memory than
 * the plain thumbnail.
 *
 * @CrosswordImage(
 *   id = "numbered_thumbnail",
 *   title = @Translation("Numbered Thumbnail"),
 *   type = IMAGETYPE_PNG,
 *   toolkit = "gd"
 * )
 */
class CrosswordNumberedThumbnail extends CrosswordThumbnailBase {

  /**
   * {@inheritdoc}
   */
  protected $squareSize = 40;

  /**
   * {@inheritdoc}
   */
  protected $lineSize = 3;

  /**
   * The font to be used by imagestring().
   *
   * @var int
   *
   * @see https://www.php.net/manual/en/function.imagestring.php
   */
  protected $font = 3;

  /**
   * {@inheritdoc}
   */
  public function createImageResource(FileInterface $file) {
    $data = $this->crosswordDataService->getData($file);
    $grid = $data['puzzle']['grid'];
    $image = $this->buildGrid($grid);
    $this->addEmbeddedImages($grid, $image);
    $this->addNumerals($grid, $image);
    $this->addCircles($grid, $image);
    $this->addHints($grid, $image);
    return $image;
  }

}
