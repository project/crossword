<?php

namespace Drupal\crossword_image\Plugin\crossword\crossword_image;

use Drupal\file\FileInterface;

/**
 * This makes a image of the solved puzzle.
 *
 * @CrosswordImage(
 *   id = "solution_thumbnail",
 *   title = @Translation("Solution Thumbnail"),
 *   type = IMAGETYPE_PNG,
 *   toolkit = "gd"
 * )
 */
class CrosswordSolutionThumbnail extends CrosswordThumbnailBase {

  /**
   * {@inheritdoc}
   */
  protected $squareSize = 40;

  /**
   * {@inheritdoc}
   */
  protected $lineSize = 3;

  /**
   * {@inheritdoc}
   */
  public function createImageResource(FileInterface $file) {
    $data = $this->crosswordDataService->getData($file);
    $grid = $data['puzzle']['grid'];
    $image = $this->buildGrid($grid);
    $this->addEmbeddedImages($grid, $image);
    $this->addCircles($grid, $image);
    $this->addFill($grid, $image);
    return $image;
  }

}
