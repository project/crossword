<?php

namespace Drupal\crossword_image\Plugin\crossword\crossword_image;

/**
 * A default thumbnail plugin.
 *
 * @CrosswordImage(
 *   id = "thumbnail",
 *   title = @Translation("Thumbnail"),
 *   type = IMAGETYPE_PNG,
 *   toolkit = "gd"
 * )
 */
class CrosswordThumbnail extends CrosswordThumbnailBase {

  /**
   * {@inheritdoc}
   */
  protected $squareSize = 20;

  /**
   * {@inheritdoc}
   */
  protected $lineSize = 2;

}
