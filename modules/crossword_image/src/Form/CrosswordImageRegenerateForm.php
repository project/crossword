<?php

namespace Drupal\crossword_image\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Regenerate crossword images.
 */
class CrosswordImageRegenerateForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'crossword_image_regenerate_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form[] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => $this->t('Regenerate Crossword Images'),
    ];
    $form[] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('If you have made changes to crossword_image plugins, submit this form to regenerate
        crossword images. For performance reasons, crossword images are created once and never updated.
        If you make a change to a crossword_image plugin, existing crossword images will not be updated. That is,
        unless you use this form!'),
    ];
    $form[] = [
      '#type' => 'container',
      '#markup' => $this->t('Back up files before attempting this on a production site.'),
      '#attributes' => [
        'class' => ['messages messages--warning'],
      ],
    ];
    $form[] = [
      '#type' => 'container',
      '#markup' => $this->t('This feature is somewhat EXPERIMENTAL and should ideally be reserved for use during
        local development. It is essential if you are developing custom crossword_image plugins, but otherwise you shouldn\'t need it.'),
      '#attributes' => [
        'class' => ['messages messages--warning'],
      ],
    ];
    $form['crossword_image_plugin_ids'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Crossword Image Plugin(s)'),
      '#options' => \Drupal::service('crossword.manager.image')->getCrosswordImageOptionList(),
      '#description' => $this->t('Select the plugin(s) for which to regenerate images. This is resource intensive, so try to select as few as possible.'),
      '#required' => TRUE,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Regenerate Crossword Images'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $crossword_image_plugin_ids = array_keys(array_filter($form_state->getValue('crossword_image_plugin_ids')));
    \Drupal::service('crossword.image_service')->regenerateCrosswordImages($crossword_image_plugin_ids);
  }

}
