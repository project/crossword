CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

 This is a sub-module of crossword. This module provides services that
 create an image from a Crossword file as well as a Field Formatter
 to render a Crossword as such an image.

 It provides a blank thumbnail, numbered thumbnail, and solution thumbanil.
 Additional crossword_image plugins could be created and used.

REQUIREMENTS
------------

 It requires its parent module (crossword).

RECOMMENDED MODULES
-------------------

 None.

INSTALLATION
------------

 Install as usual, see https://www.drupal.org/node/1897420 for further
 information.

CONFIGURATION
-------------

 This module provides a Crossword Image field formatter which can be configured
 in the usual way.

TROUBLESHOOTING
---------------

 No known common issues.

MAINTAINERS
-----------

 danflanagan8 (crossword enthusiast)
