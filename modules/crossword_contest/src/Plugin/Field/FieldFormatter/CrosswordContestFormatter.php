<?php

namespace Drupal\crossword_contest\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\crossword\Plugin\Field\FieldFormatter\CrosswordFormatter;

/**
 * Plugin implementation of the 'crossword_contest' formatter.
 *
 * @FieldFormatter(
 *   id = "crossword_contest",
 *   label = @Translation("Crossword Contest"),
 *   weight = "-10",
 *   field_types = {
 *     "crossword"
 *   }
 * )
 */
class CrosswordContestFormatter extends CrosswordFormatter {

  // Note that the Solution button acts as the submit button.
  const BUTTONS = [
    'clear' => 'Clear',
    'solution' => 'Submit',
    'undo' => 'Undo',
    'redo' => 'Redo',
    'instructions' => 'Instructions',
  ];

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options = parent::defaultSettings();
    $options['errors']['show'] = FALSE;
    unset($options['congrats']);
    unset($options['redacted']);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    unset($form['errors']);
    unset($form['congrats']);
    unset($form['redacted']);
    unset($form['redacted_warning']);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [];
  }

  /**
   * {@inheritdoc}
   *
   * This forces redaction and modifies '#attached' and '#attributes'.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $this->setSetting('redacted', TRUE);
    $elements = parent::viewElements($items, $langcode);
    foreach ($elements as $delta => &$element) {
      $element['#attached']['library'][] = 'crossword_contest/crossword.contest';
      $element['#attached']['drupalSettings']['crossword']['mid'] = $items->getEntity()->id();
      // Get rid of show-errors class if erroneously present.
      if (($key = array_search('show-errors', $element['#attributes']['class'])) !== FALSE) {
        unset($element['#attributes']['class'][$key]);
      }
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   *
   * This is only designed for the crossword_contest media type.
   * Therefore we only allow it to be used there.
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This field must be on a media entity.
    $entity_type = $field_definition->getTargetEntityTypeId();
    if ($entity_type !== 'media') {
      return FALSE;
    }
    // The bundle of the media entity must be crossword_contest.
    $media_bundle = $field_definition->getTargetBundle();
    if ($media_bundle !== 'crossword_contest') {
      return FALSE;
    }
    return TRUE;
  }

}
