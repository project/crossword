(function ($, Drupal, once, drupalSettings) {

  // This overrides the default click handler on the solution button.
  Drupal.behaviors.crosswordContest = {
    attach: function (context, settings) {
      var selector = drupalSettings.crossword.selector;
      once('crossword-contest-init', selector).forEach(function(element, index) {
        var $crossword = $(element);
        var Crossword = $crossword.data("Crossword");

        once('crossword-submit-click', '.button-solution', element).forEach(function (element, index) {
          $(element).off('click').click(function (e) {
            e.preventDefault();
            if ($(this).data('confirm')) {
              var message = $(this).data('confirm');
              if (!confirm(message)) {
                return;
              }
            }
            var answer_for_url = encodeURIComponent(JSON.stringify(Crossword.getAnswers()));
            var mid = drupalSettings.crossword.mid;
            var url = 'crossword-contest/' + mid + '/' + answer_for_url;
            var submit = Drupal.ajax({
              url: Drupal.url(url),
            });
            submit.execute();
          });
        });
      });
    },
  }
})(jQuery, Drupal, once, drupalSettings);
