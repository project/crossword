CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

 This module offers a simple framework for low-stakes contests, in which
 solving a crossword puzzle allows the user to view special content. This
 is not intended for anything relating to money or anything that would not
 be considered low-stakes or anything that might have legal implications.
 It is also presented as a starting point for your own more sophisticated
 integrations.

REQUIREMENTS
------------

 It requires its parent module (crossword), as well as crossword_media.

RECOMMENDED MODULES
-------------------

 Nope.

INSTALLATION
------------

 Install as usual, see https://www.drupal.org/node/1897420 for further
 information.

CONFIGURATION
-------------

 Upon installation, this module creates a media type called crossword_contest
 and three new display modes for media: crossword_contest_play,
 crossword_contest_correct, and crossword_contest_incorrect. Here's how to
 configure the contest.

 1. Add a media reference field to a node type. Configure this field to allow
    only crossword_contest media.
 2. Configure the node display such that the new media reference field is displayed
    as a rendered entity in the crossword_contest_play view mode.

 Now we move to configuring the crossword_contest media type, which is handled
 at a basic level upon installation. You may want to customize things.

 3. The crossword_contest_play view mode is already configured to use the
    Crossword Contest field formatter. You may adjust the configuration of that field
    formatter as you see fit.
 4. Configure the crossword_contest_correct view mode as you see fit. You may add
    new fields to the crossword_contest media to suit your needs. This view mode
    will be rendered in a modal when the user submits a correct solution. Maybe it
    contains a coupon code or a link to somewhere special.
 5. Configure the crossword_contest_incorrect view mode as you see fit. You may add
    new fields to the crossword_contest media to suit your needs. This view mode
    will be rendered in a modal when the user submits an incorrect solution. Maybe
    it contains an encouraging message or a helpful hint.

 That's it! You have a contest that is 100% configurable in the UI. No coding
 required! These view modes are fully templatable though and they use all the
 normal hooks so you CAN code if you want too!


TROUBLESHOOTING
---------------

 Nothing known.

MAINTAINERS
-----------

 danflanagan8 (crossword enthusiast)
