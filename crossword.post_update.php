<?php

/**
 * @file
 * Post-update functions crossword module.
 */

use Drupal\Core\Config\Entity\ConfigEntityUpdater;
use Drupal\Core\Entity\Display\EntityDisplayInterface;

/*
 * Lots of changes to field formatters in first beta release.
 *
 *  The book formatter has been removed. Change to crossword formatter.
 * (Issue 3163352)
 *
 * Convert any instance of the download link formatters to generic file
 * to allow major refactoring of those formatters. (Issue 3157490)
 * This is a breaking change because we start "outsourcing" most of
 * the work for these formatters to a contributed module.
 *
 * Pseudofields formatter was moved to a new module. (Issue 3163351)
 *
 * Convert any instance of the crossword_thumbnail formatter
 * to use the crossword_image_rendered formatter. (Issue 3157484)
 */

/**
 * Install submodules if using certain field formatters.
 */
function crossword_post_update_musical_formatters_step_1(&$sandbox) {
  $config_entity_updater = \Drupal::classResolver(ConfigEntityUpdater::class);
  $formatter_callback = function (EntityDisplayInterface $display) {
    $needs_save = FALSE;
    foreach ($display->getComponents() as $field_name => $component) {
      if (isset($component['type'])) {
        switch ($component['type']) {
          case 'crossword_pseudofield':
            \Drupal::service('module_installer')->install(['crossword_pseudofields']);
            break;

          case 'crossword_thumbnail':
            \Drupal::service('module_installer')->install(['crossword_image']);
            break;
        }
      }
    }

    return $needs_save;
  };

  $config_entity_updater->update($sandbox, 'entity_view_display', $formatter_callback);
}

/**
 * Update formatters to account for removals and new submodules.
 */
function crossword_post_update_musical_formatters_step_2(&$sandbox) {
  $config_entity_updater = \Drupal::classResolver(ConfigEntityUpdater::class);
  /** @var \Drupal\Core\Field\FormatterPluginManager $field_formatter_manager */
  $field_formatter_manager = \Drupal::service('plugin.manager.field.formatter');
  $field_formatter_manager->clearCachedDefinitions();

  $formatter_callback = function (EntityDisplayInterface $display) {
    $needs_save = FALSE;
    foreach ($display->getComponents() as $field_name => $component) {
      if (isset($component['type'])) {
        switch ($component['type']) {
          case 'crossword_book':
            $component['type'] = 'crossword';
            $display->setComponent($field_name, $component);
            $needs_save = TRUE;
            \Drupal::messenger()->addWarning('Possible breaking change: Crossword Puzzle (Book) formatter changed to Crossword Puzzle Formatter. Please inspect your site for resulting changes.');
            \Drupal::logger('crossword')->warning('Possible breaking change: Crossword Puzzle (Book) formatter changed to Crossword Puzzle Formatter. Please inspect your site for resulting changes.');
            break;

          case 'crossword_file_download_link':
          case 'crossword_image_download':
            $component['type'] = 'file_default_crossword';
            $display->setComponent($field_name, $component);
            \Drupal::messenger()->addWarning("Possible breaking change: Instance of File Download Link or Crossword Image (download) converted to Generic File formatter. Please inspect your site for resulting changes.");
            \Drupal::logger('crossword')->warning("Possible breaking change: Instance of File Download Link or Crossword Image (download) converted to Generic File formatter. Please inspect your site for resulting changes.");
            $needs_save = TRUE;
            break;

          case 'crossword_pseudofield':
            $component['type'] = 'crossword_pseudofields';
            $display->setComponent($field_name, $component);
            $needs_save = TRUE;
            break;

          case 'crossword_thumbnail':
            $component['type'] = 'crossword_image_rendered';
            $display->setComponent($field_name, $component);
            $needs_save = TRUE;
            break;
        }

      }
    }

    return $needs_save;
  };

  $config_entity_updater->update($sandbox, 'entity_view_display', $formatter_callback);
}

/**
 * Update formatters with new button schema: Issue #3173233.
 */
function crossword_post_update_button_schema(&$sandbox) {
  $config_entity_updater = \Drupal::classResolver(ConfigEntityUpdater::class);
  /** @var \Drupal\Core\Field\FormatterPluginManager $field_formatter_manager */
  $field_formatter_manager = \Drupal::service('plugin.manager.field.formatter');

  $formatter_callback = function (EntityDisplayInterface $display) {
    $needs_save = FALSE;
    $buttons = [
      'cheat' => 'Cheat',
      'solution' => 'Solution',
      'clear' => 'Clear',
      'undo' => 'Undo',
      'redo' => 'Redo',
    ];
    foreach ($display->getComponents() as $field_name => $component) {
      if (isset($component['type']) && in_array($component['type'], ['crossword', 'crossword_screenreader', 'crossword_pseudofields'])) {
        $settings = &$component['settings'];
        foreach ($settings['buttons']['buttons'] as $key => $val) {
          $settings['buttons']['buttons'][$key] = [
            'show' => $val != FALSE,
            'input_label' => $buttons[$key],
          ];
          if (isset($settings['rebus'])) {
            $settings['rebus']['input_label'] = 'Rebus entry active';
          }
          if (isset($settings['errors'])) {
            $settings['errors']['input_label'] = 'Show Errors';
          }
          if (isset($settings['references'])) {
            $settings['references']['input_label'] = 'Show References';
          }
        }
        $display->setComponent($field_name, $component);
        $needs_save = TRUE;
        break;
      }
    }

    return $needs_save;
  };

  $config_entity_updater->update($sandbox, 'entity_view_display', $formatter_callback);
}

/**
 * Update formatters with default confirmation text: Issue #3190407.
 */
function crossword_post_update_button_confirm(&$sandbox) {
  $config_entity_updater = \Drupal::classResolver(ConfigEntityUpdater::class);
  /** @var \Drupal\Core\Field\FormatterPluginManager $field_formatter_manager */
  $field_formatter_manager = \Drupal::service('plugin.manager.field.formatter');

  $formatter_callback = function (EntityDisplayInterface $display) {
    $needs_save = FALSE;
    foreach ($display->getComponents() as $field_name => $component) {
      if (isset($component['type']) && in_array($component['type'], ['crossword', 'crossword_screenreader', 'crossword_pseudofields'])) {
        $settings = &$component['settings'];
        if (isset($settings['buttons']['buttons']['clear']) && !isset($settings['buttons']['buttons']['clear']['confirm'])) {
          $settings['buttons']['buttons']['clear']['confirm'] = 'Do you really want to clear? This action cannot be undone.';
        }
        if (isset($settings['buttons']['buttons']['solution']) && !isset($settings['buttons']['buttons']['solution']['confirm'])) {
          $settings['buttons']['buttons']['solution']['confirm'] = 'Do you really want to give up?';
        }
        $display->setComponent($field_name, $component);
        $needs_save = TRUE;
        break;
      }
    }

    return $needs_save;
  };

  $config_entity_updater->update($sandbox, 'entity_view_display', $formatter_callback);
}

/**
 * Remove crossword_screenreader formatter: Issue 3201710.
 */
function crossword_post_update_musical_formatters_step_3(&$sandbox) {
  $config_entity_updater = \Drupal::classResolver(ConfigEntityUpdater::class);
  /** @var \Drupal\Core\Field\FormatterPluginManager $field_formatter_manager */
  $field_formatter_manager = \Drupal::service('plugin.manager.field.formatter');
  $field_formatter_manager->clearCachedDefinitions();

  $formatter_callback = function (EntityDisplayInterface $display) {
    $needs_save = FALSE;
    foreach ($display->getComponents() as $field_name => $component) {
      if (isset($component['type'])) {
        switch ($component['type']) {
          case 'crossword_screenreader':
            $component['type'] = 'crossword';
            $display->setComponent($field_name, $component);
            $needs_save = TRUE;
            \Drupal::messenger()->addWarning('Possible breaking change: Crossword Puzzle (Screenreader) formatter changed to Crossword Puzzle Formatter. Please inspect your site for resulting changes.');
            \Drupal::logger('crossword')->warning('Possible breaking change: Crossword Puzzle (Screenreader) formatter changed to Crossword Puzzle Formatter. Please inspect your site for resulting changes.');
            break;
        }
      }
    }

    return $needs_save;
  };

  $config_entity_updater->update($sandbox, 'entity_view_display', $formatter_callback);
}

/**
 * Clear caches so that Crossword cache gets rebuilt with XSS filtering.
 */
function crossword_post_update_tags_in_clues() {
  // Empty post-update hook.
}
