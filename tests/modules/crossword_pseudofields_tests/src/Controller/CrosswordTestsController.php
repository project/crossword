<?php

namespace Drupal\crossword_pseudofields_tests\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class CrosswordTestsController.
 */
class CrosswordTestsController extends ControllerBase {

  /**
   * Renders the crossword with a specified formatter.
   *
   * @param int $id
   *   The node id.
   * @param string $formatter
   *   Machine name of the formatter to use. Only here for symmetry.
   * @param string $redacted
   *   Use 'redacted' to redact the solution.
   *
   * @return array
   *   The build array.
   */
  public function build($id, $formatter, $redacted) {
    if ($node = Node::load($id)) {
      if ($node->access('view', $this->currentUser())) {
        if ($redacted == 'redacted') {
          $config = \Drupal::config('crossword_pseudofields.settings');
          $settings = $config->get();
          $config = \Drupal::service('config.factory')->getEditable('crossword_pseudofields.settings');
          $config->set('redacted', TRUE)->save();
        }
        return \Drupal::entityTypeManager()->getViewBuilder('node')->view($node, 'default');
      }
      else {
        throw new AccessDeniedHttpException();
      }
    }
    else {
      throw new NotFoundHttpException();
    }
  }

}
