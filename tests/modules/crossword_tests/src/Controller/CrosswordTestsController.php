<?php

namespace Drupal\crossword_tests\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class CrosswordTestsController.
 */
class CrosswordTestsController extends ControllerBase {

  /**
   * Renders the crossword with a specified formatter.
   *
   * @param int $id
   *   The node id.
   * @param string $formatter
   *   Machine name of the formatter to use.
   * @param string $redacted
   *   Use 'redacted' to redact the solution.
   *
   * @return array
   *   The build array.
   */
  public function build($id, $formatter, $redacted) {
    if ($node = Node::load($id)) {
      if ($node->access('view', $this->currentUser())) {
        return $node->field_crossword->view([
          'type' => $formatter,
          'label' => 'hidden',
          'settings' => [
            'redacted' => ($redacted == 'redacted') ? TRUE : FALSE,
          ],
        ]);
      }
      else {
        throw new AccessDeniedHttpException();
      }
    }
    else {
      throw new NotFoundHttpException();
    }
  }

}
