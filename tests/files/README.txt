Here's some info about how test files were created. First, I made
text.txt manually in a text editor. To create text.puz I opened
text.txt in AcrossLite and then saved it as a puz file.

I created various corrupted files by editing these initial test
files. With the .txt files, editing was easy. For the puz files
it's not as obvious. I opened text.puz in Atom in ISO-8859-1 encoding
with Atom's whitespace extensions disabled. Then I was able to
edit it in a few ways. Most characters were still invisible to me.
Since the puz files are hard to read, here's what I did to them:

=bad_grid.puz=
Has a black square in the very middle of grid. This reduces the
calculated clues down to 3.

=just_enough_to_apply.puz=
Everything after ACROSS&DOWN has been deleted. This puzzle should pass
the initial applicable test but fail parsing miserably.

=clue_list_deleted.puz=
All of the clues have been deleted. Everything else is untouched.

Some of the files included were associated with issues on Drupal.org.
The authors gave permission for their inclusion for testing.

3185956.puz by GNUMatrix
3102646.txt by saweyer
3102647.txt by saweyer

One of the ipuz examples is from ipuz.org/example.

