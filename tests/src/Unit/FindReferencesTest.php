<?php

namespace Drupal\Tests\crossword\Unit;

use Drupal\crossword\CrosswordDataService;
use Drupal\Tests\UnitTestCase;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Tests finding references with CrosswordDataService.
 *
 * @group crossword
 */
class FindReferencesTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * Test findReferences function.
   *
   * @dataProvider findReferencesProvider
   */
  public function testFindReferences($text) {
    /** @var \Drupal\crossword\CrosswordDataService $crossword_data_service */
    $crossword_data_service = $this->prophesize(CrosswordDataService::class);
    $crossword_data_service = $crossword_data_service->reveal();

    $class = new \ReflectionClass(CrosswordDataService::class);
    $instance_method = $class->getMethod('findReferences');
    $instance_method->setAccessible(TRUE);

    $expected = [
      [
        'dir' => 'across',
        'numeral' => '12',
      ],
      [
        'dir' => 'across',
        'numeral' => '57',
      ],
      [
        'dir' => 'down',
        'numeral' => '34',
      ],
    ];

    $this->assertEqualsCanonicalizing($expected, $instance_method->invoke($crossword_data_service, $text));
  }

  /**
   * Provider for testFindReferences().
   *
   * @return array
   *   Structured data for tests.
   */
  public static function findReferencesProvider() {
    return [
      'nyt' => ['text' => 'Common feature of 12- and 57-Across and 34-Down'],
      'verbose' => ['text' => 'Common feature of 12-Across and 57-Across and 34-Down'],
      'verbose2' => ['text' => 'Common feature of 12-Across, 34-Down, and 57-Across'],
      'medium' => ['text' => 'Common feature of 12-A and 57-A and 34-D'],
      'medium2' => ['text' => 'Common feature of 12-A, 34-D, and 57-A'],
      'short' => ['text' => 'Common feature of 12A and 57A and 34D'],
      'short2' => ['text' => 'Common feature of 12A, 34D, and 57A'],
      'lower' => ['text' => 'Common feature of 12- and 57-across and 34-down'],
      'mixed' => ['text' => 'Common feature of 12-a and 57-Across and 34D'],
    ];
  }

  /**
   * Tests addReferences specifically with "starred clues".
   */
  public function testStarredClues() {
    /** @var \Drupal\crossword\CrosswordDataService $crossword_data_service */
    $crossword_data_service = $this->prophesize(CrosswordDataService::class);
    $crossword_data_service = $crossword_data_service->reveal();

    $class = new \ReflectionClass(CrosswordDataService::class);
    $instance_method = $class->getMethod('addReferences');
    $instance_method->setAccessible(TRUE);

    $data = [
      'puzzle' => [
        'clues' => [
          'across' => [
            [
              'numeral' => 1,
              'text' => 'This clues has *',
            ],
            [
              'numeral' => 2,
              'text' => 'Common feature of the starred clues.',
            ],
          ],
          'down' => [
            [
              'numeral' => 3,
              'text' => 'No star here but a reference to 2A.',
            ],
            [
              'numeral' => 4,
              'text' => '* me kitten',
            ],
          ],
        ],
      ],
    ];

    $expected = [
      'puzzle' => [
        'clues' => [
          'across' => [
            [
              'numeral' => 1,
              'text' => 'This clues has *',
              'references' => NULL,
            ],
            [
              'numeral' => 2,
              'text' => 'Common feature of the starred clues.',
              'references' => [
                [
                  'dir' => 'across',
                  'numeral' => 1,
                  'index' => 0,
                ],
                [
                  'dir' => 'down',
                  'numeral' => 4,
                  'index' => 1,
                ],
              ],
            ],
          ],
          'down' => [
            [
              'numeral' => 3,
              'text' => 'No star here but a reference to 2A.',
              'references' => [
                [
                  'dir' => 'across',
                  'numeral' => '2',
                  'index' => 1,
                ],
              ],
            ],
            [
              'numeral' => 4,
              'text' => '* me kitten',
              'references' => NULL,
            ],
          ],
        ],
      ],
    ];

    $this->assertEqualsCanonicalizing($expected, $instance_method->invoke($crossword_data_service, $data));
  }

}
