<?php

namespace Drupal\Tests\crossword\FunctionalJavascript;

use Drupal\crossword_media\Plugin\media\Source\Crossword;
use Drupal\media\Entity\Media;
use Drupal\Tests\media\FunctionalJavascript\MediaSourceTestBase;

/**
 * Tests the crossword media source.
 *
 * The plugin is a simple extension of the file media source. So this
 * test started as a copy-paste of MediaSourceFileTest, which I then
 * edited to suit the crossword media source.
 *
 * @group crossword
 */
class MediaSourceCrosswordTest extends MediaSourceTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'crossword_media',
  ];

  /**
   * Tests the crossword media source.
   */
  public function testMediaCrosswordSource() {
    $media_type_id = 'test_media_crossword_type';
    $source_field_id = 'field_media_crossword';
    $provided_fields = [
      Crossword::METADATA_ATTRIBUTE_NAME,
      Crossword::METADATA_ATTRIBUTE_SIZE,
      Crossword::METADATA_ATTRIBUTE_MIME,
    ];

    $session = $this->getSession();
    $page = $session->getPage();
    $assert_session = $this->assertSession();

    $this->doTestCreateMediaType($media_type_id, 'crossword', $provided_fields);

    // Create custom fields for the media type to store metadata attributes.
    $fields = [
      'field_string_file_size' => 'string',
      'field_string_mime_type' => 'string',
    ];
    $this->createMediaTypeFields($fields, $media_type_id);

    // Hide the name field widget to test default name generation.
    $this->hideMediaTypeFieldWidget('name', $media_type_id);

    $this->drupalGet("admin/structure/media/manage/{$media_type_id}");
    $page->selectFieldOption("field_map[" . Crossword::METADATA_ATTRIBUTE_NAME . "]", 'name');
    $page->selectFieldOption("field_map[" . Crossword::METADATA_ATTRIBUTE_SIZE . "]", 'field_string_file_size');
    $page->selectFieldOption("field_map[" . Crossword::METADATA_ATTRIBUTE_MIME . "]", 'field_string_mime_type');
    $page->pressButton('Save');

    $test_filename = 'test.txt';
    $test_filepath = 'public://' . $test_filename;
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/" . $test_filename);
    file_put_contents($test_filepath, $contents);

    // Create a media item.
    $this->drupalGet("media/add/{$media_type_id}");
    $page->attachFileToField("files[{$source_field_id}_0]", \Drupal::service('file_system')->realpath($test_filepath));
    $result = $assert_session->waitForButton('Remove');
    $this->assertNotEmpty($result);
    $page->pressButton('Save');

    // We should be on the media listing view.
    $assert_session->addressEquals('admin/content/media');
    $assert_session->statusMessageContains("test_media_crossword_type $test_filename has been created");
    $assert_session->elementExists('css', 'img[src*="/crossword/1-thumbnail.png"]');

    // Load the media and check that all the fields are properly populated.
    $media = Media::load(1);
    $this->assertSame($test_filename, $media->getName());
    $this->assertSame('313', $media->get('field_string_file_size')->value);
    $this->assertSame('text/plain', $media->get('field_string_mime_type')->value);
    // Check that the thumbnail_uri is correct.
    $thumbnail_uri = $media->getSource()->getMetadata($media, 'thumbnail_uri');
    $this->assertFileExists($thumbnail_uri);
    $this->assertTrue(strpos($thumbnail_uri, '/crossword/1-thumbnail.png') > -1);
  }

}
