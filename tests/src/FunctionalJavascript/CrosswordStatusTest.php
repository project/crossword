<?php

namespace Drupal\Tests\crossword\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\node\Entity\Node;

/**
 * Tests functionality of crossword status.
 *
 * @group crossword
 */
class CrosswordStatusTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'crossword_tests',
    'crossword_status',
  ];

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Name of test puzzle.
   *
   * @var string
   */
  protected $testPuzzleFilename = 'test.txt';

  /**
   * Test the crossword status js.
   */
  public function testCrosswordStatus() {

    // Create a couple nodes.
    $node_test = $this->createTestNode();
    $this->testPuzzleFilename = 'xss.txt';
    $node_xss = $this->createTestNode();

    // Check out the frontpage view.
    $this->drupalGet('<front>');
    $assertSession = $this->assertSession();
    $session = $this->getSession();
    $page = $session->getPage();
    $assertSession->elementExists('css', '.crossword-status-none[data-crossword-fid="1"]');
    $assertSession->elementNotExists('css', '.crossword-status-revealed[data-crossword-fid="1"]');
    $assertSession->elementNotExists('css', '.crossword-status-solved[data-crossword-fid="1"]');
    $assertSession->elementExists('css', '.crossword-status-none[data-crossword-fid="2"]');
    $assertSession->elementNotExists('css', '.crossword-status-in-progress[data-crossword-fid="2"]');

    // View crossword 1 and reveal it.
    $this->drupalGet('node/1');
    $page->find('css', '.button-solution')->click();
    $session->getDriver()->getWebDriverSession()->accept_alert();

    // View frontpage again and see that status has updated.
    $this->drupalGet('<front>');
    $assertSession = $this->assertSession();
    $session = $this->getSession();
    $page = $session->getPage();
    $assertSession->elementNotExists('css', '.crossword-status-none[data-crossword-fid="1"]');
    $assertSession->elementExists('css', '.crossword-status-revealed[data-crossword-fid="1"]');
    $assertSession->elementNotExists('css', '.crossword-status-solved[data-crossword-fid="1"]');
    $assertSession->elementExists('css', '.crossword-status-none[data-crossword-fid="2"]');
    $assertSession->elementNotExists('css', '.crossword-status-in-progress[data-crossword-fid="2"]');

    // View crossword 2 and start it.
    $this->drupalGet('node/2');
    $page->find('css', '.button-cheat')->click();

    // View frontpage again and see that status has updated.
    $this->drupalGet('<front>');
    $assertSession = $this->assertSession();
    $session = $this->getSession();
    $page = $session->getPage();
    $assertSession->elementNotExists('css', '.crossword-status-none[data-crossword-fid="1"]');
    $assertSession->elementExists('css', '.crossword-status-revealed[data-crossword-fid="1"]');
    $assertSession->elementNotExists('css', '.crossword-status-solved[data-crossword-fid="1"]');
    $assertSession->elementNotExists('css', '.crossword-status-none[data-crossword-fid="2"]');
    $assertSession->elementExists('css', '.crossword-status-in-progress[data-crossword-fid="2"]');

    // View crossword 1 and clear it.
    $this->drupalGet('node/1');
    $page->find('css', '.button-clear')->click();
    $session->getDriver()->getWebDriverSession()->accept_alert();

    // View frontpage again and see that status has updated.
    $this->drupalGet('<front>');
    $assertSession = $this->assertSession();
    $session = $this->getSession();
    $page = $session->getPage();
    $assertSession->elementExists('css', '.crossword-status-none[data-crossword-fid="1"]');
    $assertSession->elementNotExists('css', '.crossword-status-revealed[data-crossword-fid="1"]');
    $assertSession->elementNotExists('css', '.crossword-status-solved[data-crossword-fid="1"]');
    $assertSession->elementNotExists('css', '.crossword-status-none[data-crossword-fid="2"]');
    $assertSession->elementExists('css', '.crossword-status-in-progress[data-crossword-fid="2"]');

    // View crossword 1 and solve it.
    $this->drupalGet('node/1');
    // Enter all correct answers (by cheating).
    $page->find('css', '[data-col="0"][data-row="0"]')->click();
    $page->find('css', '.button-cheat')->click();
    $page->find('css', '.button-cheat')->click();
    $page->find('css', '.button-cheat')->click();
    $page->find('css', '.button-cheat')->click();
    $page->find('css', '.button-cheat')->click();
    $page->find('css', '.button-cheat')->click();
    $page->find('css', '.button-cheat')->click();
    $page->find('css', '.button-cheat')->click();

    // View frontpage again and see that status has updated.
    $this->drupalGet('<front>');
    $assertSession = $this->assertSession();
    $session = $this->getSession();
    $page = $session->getPage();
    $assertSession->elementNotExists('css', '.crossword-status-none[data-crossword-fid="1"]');
    $assertSession->elementNotExists('css', '.crossword-status-revealed[data-crossword-fid="1"]');
    $assertSession->elementExists('css', '.crossword-status-solved[data-crossword-fid="1"]');
    $assertSession->elementNotExists('css', '.crossword-status-none[data-crossword-fid="2"]');
    $assertSession->elementExists('css', '.crossword-status-in-progress[data-crossword-fid="2"]');

    // View crossword 1 and start it. See that solved status persists,
    // namely not in-progress.
    $this->drupalGet('node/1');
    $page->find('css', '.button-cheat')->click();

    // View frontpage again and see that status has updated.
    $this->drupalGet('<front>');
    $assertSession = $this->assertSession();
    $session = $this->getSession();
    $page = $session->getPage();
    $assertSession->elementNotExists('css', '.crossword-status-none[data-crossword-fid="1"]');
    $assertSession->elementNotExists('css', '.crossword-status-revealed[data-crossword-fid="1"]');
    $assertSession->elementExists('css', '.crossword-status-solved[data-crossword-fid="1"]');
    $assertSession->elementNotExists('css', '.crossword-status-none[data-crossword-fid="2"]');
    $assertSession->elementExists('css', '.crossword-status-in-progress[data-crossword-fid="2"]');
  }

  /**
   * Helper function to create node that can be viewed and used for testing.
   *
   * @return Drupal\node\Entity\Node
   *   A crossword node that can be used in tests.
   */
  protected function createTestNode() {
    // First we move a test file to the file system.
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/{$this->testPuzzleFilename}");
    $file = \Drupal::service('file.repository')->writeData($contents, "public://{$this->testPuzzleFilename}");
    // Now use that file in a new crossword node.
    $node = Node::create(['type' => 'crossword']);
    $node->set('title', 'Test Crossword Node');
    $node->set('field_crossword', $file->id());
    $node->set('status', 1);
    $node->save();
    return $node;
  }

}
