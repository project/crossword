<?php

namespace Drupal\Tests\crossword\FunctionalJavascript;

/**
 * Tests numerous XSS attempts against Crossword Solution Formatter.
 *
 * @group crossword
 */
class CrosswordSolutionXssTest extends CrosswordFormatterTestBase {

  /**
   * {@inheritdoc}
   */
  protected $formatter = 'crossword_solution';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['crossword_tests'];

  /**
   * {@inheritdoc}
   */
  protected $testPuzzleFilename = 'xss.txt';

  /**
   * Test Xss in crossword field formatter plugin.
   */
  public function testCrosswordFormatter() {

    $node = $this->createTestNode();
    $this->assertEquals(1, $node->id());

    // View the crossword node.
    $this->drupalGet("crossword-tests/1/{$this->formatter}");
    $assertSession = $this->assertSession();
    $session = $this->getSession();
    $page = $session->getPage();

    // These is attempted XSS all over this puzzle. If it causes an alert
    // to appear at any time, there would end up being an UnexpectedAlertOpen
    // and the test would err out. If we get to the end and everything passes
    // then we know no alert ever appeared. Let's start!
    $assertSession->pageTextContains("alert('Xss Title');");
    $assertSession->pageTextContains("alert('Xss Author');");
    $assertSession->pageTextContains("alert('Xss Notepad');");
    $assertSession->pageTextContains("ALERT('XSS LETTER');");

    // Just for fun, do an active test of alert presence.
    try {
      $session->getDriver()->getWebDriverSession()->getAlert_text();
      $exception = NULL;
    }
    catch (\Exception $e) {
      $exception = $e;
    }
    $this->assertEquals('WebDriver\Exception\NoAlertOpenError', get_class($exception));
  }

}
