<?php

namespace Drupal\Tests\crossword\FunctionalJavascript;

/**
 * Tests numerous XSS attempts against Crossword Formatter.
 *
 * I'm always nervous that something with this much user input
 * could be exploited by XSS. In the xss.txt puzzle I try to
 * add alerts anywhere and everywhere. This class is supposed to
 * test whether they fire or get properly escaped.
 *
 * @group crossword
 */
class CrosswordFormatterXssTest extends CrosswordFormatterTestBase {

  /**
   * {@inheritdoc}
   */
  protected $formatter = 'crossword';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['crossword_tests'];

  /**
   * {@inheritdoc}
   */
  protected $testPuzzleFilename = 'xss.txt';

  /**
   * Test Xss in crossword field formatter plugin.
   */
  public function testCrosswordFormatter() {

    $node = $this->createTestNode();
    $this->assertEquals(1, $node->id());

    // View the crossword node.
    $this->drupalGet("crossword-tests/1/{$this->formatter}");
    $assertSession = $this->assertSession();
    $session = $this->getSession();
    $page = $session->getPage();

    // These is attempted XSS all over this puzzle. If it causes an alert
    // to appear at any time, there would end up being an UnexpectedAlertOpen
    // and the test would err out. If we get to the end and everything passes
    // then we know no alert ever appeared. Let's start!
    // Make sure the attempted Xss text is on the page.
    $assertSession->pageTextContains("alert('Xss Title');");
    $assertSession->pageTextContains("alert('Xss Author');");
    $assertSession->pageTextContains("alert('Xss Notepad');");
    $assertSession->pageTextContains("alert('Xss Clue');");
    $assertSession->pageTextContains("alert('Xss Reference');");

    // Click on top right square and cheat.
    // This square is un-crossed so direction changes to down.
    $square = $page->find('css', '[data-col="2"][data-row="0"]');
    $square->click();
    $page->find('css', '.button-cheat')->click();
    $assertSession->pageTextContains("alert('Xss Letter');");

    // Click (twice) on 3-Across and see clue.
    $square = $page->find('css', '[data-col="0"][data-row="1"]');
    $square->click();
    $square->click();
    $assertSession->elementTextContains('css', '#active-clues', "alert('Xss Clue');");

    // Click on 5-Across and see reference.
    $square = $page->find('css', '[data-col="0"][data-row="2"]');
    $square->click();
    $assertSession->elementTextContains('css', '#active-clues', "alert('Xss Reference');");

    // Just for fun, do an active test of alert presence.
    try {
      $session->getDriver()->getWebDriverSession()->getAlert_text();
      $exception = NULL;
    }
    catch (\Exception $e) {
      $exception = $e;
    }
    $this->assertEquals('WebDriver\Exception\NoAlertOpenError', get_class($exception));

    // Reload to examine local storage.
    $this->drupalGet("crossword-tests/1/{$this->formatter}");
    $assertSession = $this->assertSession();
    $session = $this->getSession();
    $assertSession->pageTextContains("alert('Xss Letter');");
    try {
      $session->getDriver()->getWebDriverSession()->getAlert_text();
      $exception = NULL;
    }
    catch (\Exception $e) {
      $exception = $e;
    }
    $this->assertEquals('WebDriver\Exception\NoAlertOpenError', get_class($exception));
  }

}
