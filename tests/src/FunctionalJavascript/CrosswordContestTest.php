<?php

namespace Drupal\Tests\crossword\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;

/**
 * Tests functionality of crossword_contest.
 *
 * @group crossword
 */
class CrosswordContestTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'crossword_contest_tests',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Name of test puzzle.
   *
   * @var string
   */
  protected $testPuzzleFilename = 'test.txt';

  /**
   * Test the crossword playing a crossword contest..
   */
  public function testCrosswordContest() {
    $this->createTestNode();
    $this->drupalLogin($this->drupalCreateUser([
      'access content',
      'view media',
    ]));
    $this->drupalGet('node/1');
    $assert_session = $this->assertSession();
    $session = $this->getSession();
    $page = $session->getPage();
    // Submit bad answer.
    // Usually "cheating" is the easiest way to enter answers.
    // We don't have cheating available to us.
    $page->find('css', '[data-col="0"][data-row="0"]')->click();
    $page->find('css', '[data-col="0"][data-row="0"] input')->setValue('A');
    $page->find('css', '[data-col="2"][data-row="0"]')->click();
    $page->find('css', '.rebus-entry')->click();
    $page->find('css', '[data-col="2"][data-row="0"] input')->setValue('O');
    $page->find('css', '[data-col="2"][data-row="0"] input')->setValue('N');
    $page->find('css', '[data-col="2"][data-row="0"] input')->setValue('E');
    $page->find('css', '.rebus-entry')->click();
    $page->find('css', '[data-col="0"][data-row="1"]')->click();
    $page->find('css', '[data-col="0"][data-row="1"] input')->setValue('B');
    $page->find('css', '[data-col="1"][data-row="1"]')->click();
    $page->find('css', '[data-col="1"][data-row="1"] input')->setValue('C');
    $page->find('css', '[data-col="2"][data-row="1"]')->click();
    $page->find('css', '[data-col="2"][data-row="1"] input')->setValue('D');
    $page->find('css', '[data-col="0"][data-row="2"]')->click();
    $page->find('css', '.rebus-entry')->click();
    $page->find('css', '[data-col="0"][data-row="2"] input')->setValue('T');
    $page->find('css', '[data-col="0"][data-row="2"] input')->setValue('W');
    $page->find('css', '[data-col="0"][data-row="2"] input')->setValue('O');
    $page->find('css', '.rebus-entry')->click();
    $page->find('css', '[data-col="1"][data-row="2"]')->click();
    $page->find('css', '[data-col="1"][data-row="2"] input')->setValue('E');
    $page->find('css', '[data-col="2"][data-row="2"]')->click();
    $page->find('css', '[data-col="2"][data-row="2"] input')->setValue('X');
    $page->find('css', '.button-solution')->click();
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementTextContains('css', '#drupal-modal', 'Not quite...keep at it!');
    $assert_session->elementTextNotContains('css', '#drupal-modal', 'Well done!');
    $page->find('css', '.ui-dialog-titlebar-close')->click();
    // Confirm modal closed.
    $assert_session->pageTextNotContains('Not quite...keep at it!');

    // Submit correct answer.
    $page->find('css', '[data-col="2"][data-row="2"]')->click();
    $page->find('css', '[data-col="2"][data-row="2"] input')->setValue('F');
    $page->find('css', '.button-solution')->click();
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementTextNotContains('css', '#drupal-modal', 'Not quite...keep at it!');
    $assert_session->elementTextContains('css', '#drupal-modal', 'Well done!');
  }

  /**
   * Helper function to create node that can be viewed and used for testing.
   */
  protected function createTestNode() {
    // First we move a test file to the file system.
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/{$this->testPuzzleFilename}");
    $file = \Drupal::service('file.repository')->writeData($contents, "public://{$this->testPuzzleFilename}");
    // Now use that file in a new crossword_contests media.
    $media = Media::create(['bundle' => 'crossword_contest']);
    $media->set('name', 'Test Crossword Contest');
    $media->set('field_crossword_contest_xword', $file->id());
    $media->set('field_incorrect_message', 'Not quite...keep at it!');
    $media->set('field_correct_message', 'Well done!');
    $media->set('status', 1);
    $media->save();
    // Now put that media on a node.
    $node = Node::create(['type' => 'contest_node']);
    $node->set('title', 'Test Contest Node');
    $node->set('field_contest_media', 1);
    $node->set('status', 1);
    $node->save();
  }

}
