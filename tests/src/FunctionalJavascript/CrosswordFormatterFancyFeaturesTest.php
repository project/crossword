<?php

namespace Drupal\Tests\crossword\FunctionalJavascript;

/**
 * Tests that xml files can have hints and images.
 *
 * @group crossword
 */
class CrosswordFormatterFancyFeaturesTest extends CrosswordFormatterTestBase {

  /**
   * {@inheritdoc}
   */
  protected $formatter = 'crossword';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['crossword_tests'];

  /**
   * {@inheritdoc}
   */
  protected $testPuzzleFilename = 'test_fancy_features.xml';

  /**
   * Test that the hint shows up.
   */
  public function testCrosswordFormatter() {

    $node = $this->createTestNode();
    $this->assertEquals(1, $node->id());

    // View the crossword node.
    $this->drupalGet("crossword-tests/1/{$this->formatter}");
    $assertSession = $this->assertSession();

    // The bottom left square and middle square have a hint.
    $assertSession->elementTextNotContains('css', '[data-col="0"][data-row="0"] .square-fill', 'A');
    $assertSession->elementTextNotContains('css', '[data-col="2"][data-row="0"] .square-fill', 'ONE');
    $assertSession->elementTextNotContains('css', '[data-col="0"][data-row="1"] .square-fill', 'B');
    $assertSession->elementTextContains('css', '[data-col="1"][data-row="1"] .square-fill', 'C');
    $assertSession->elementTextNotContains('css', '[data-col="2"][data-row="1"] .square-fill', 'D');
    $assertSession->elementTextContains('css', '[data-col="0"][data-row="2"] .square-fill', 'TWO');
    $assertSession->elementTextNotContains('css', '[data-col="1"][data-row="2"] .square-fill', 'E');
    $assertSession->elementTextNotContains('css', '[data-col="2"][data-row="2"] .square-fill', 'F');

    // The black square should have an img tag inside it.
    $session = $this->getSession();
    $page = $session->getPage();
    $img = $page->find('css', '[data-col="1"][data-row="0"] img.crossword-embedded-image.width-1.height-1');
    $expected_src = 'data:image/PNG;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==';
    $this->assertSame($expected_src, $img->getAttribute('src'));
  }

  /**
   * Hints should show up even when solution is redacted.
   */
  public function testCrosswordFormatterRedacted() {

    $node = $this->createTestNode();
    $this->assertEquals(1, $node->id());

    // View the crossword node.
    $this->drupalGet("crossword-tests/1/{$this->formatter}/redacted");
    $assertSession = $this->assertSession();

    // The bottom left square and middle square have a hint.
    $assertSession->elementTextNotContains('css', '[data-col="0"][data-row="0"] .square-fill', 'A');
    $assertSession->elementTextNotContains('css', '[data-col="2"][data-row="0"] .square-fill', 'ONE');
    $assertSession->elementTextNotContains('css', '[data-col="0"][data-row="1"] .square-fill', 'B');
    $assertSession->elementTextContains('css', '[data-col="1"][data-row="1"] .square-fill', 'C');
    $assertSession->elementTextNotContains('css', '[data-col="2"][data-row="1"] .square-fill', 'D');
    $assertSession->elementTextContains('css', '[data-col="0"][data-row="2"] .square-fill', 'TWO');
    $assertSession->elementTextNotContains('css', '[data-col="1"][data-row="2"] .square-fill', 'E');
    $assertSession->elementTextNotContains('css', '[data-col="2"][data-row="2"] .square-fill', 'F');
  }

}
