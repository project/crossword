<?php

namespace Drupal\Tests\crossword\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests crossword_colors form and service.
 *
 * @group crossword
 */
class CrosswordColorsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['crossword_colors'];

  /**
   * {@inheritdoc}
   */
  public $defaultTheme = 'stark';

  /**
   * Use form to update config and confirm stylesheet is saved/updated.
   */
  public function testCrosswordColorsForm() {
    $this->assertFileDoesNotExist('public://crossword-colors.css');
    $this->drupalLogin($this->drupalCreateUser(['configure crossword colors']));

    // Save config for first time.
    $this->drupalGet('/admin/config/crossword/colors');
    $this->submitForm(['active_square' => '#123456'], 'Save configuration');
    $this->assertFileExists('public://crossword-colors.css');
    $stylesheet = file_get_contents('public://crossword-colors.css');
    $needle = '.crossword-square.active {background-color: #123456;}';
    $this->assertTrue(strpos($stylesheet, $needle) > -1);

    // Re-save with new color and make sure the file updates.
    $this->drupalGet('/admin/config/crossword/colors');
    $this->submitForm(['active_square' => '#abcdef'], 'Save configuration');
    $stylesheet = file_get_contents('public://crossword-colors.css');
    $new_needle = '.crossword-square.active {background-color: #abcdef;}';
    $this->assertFalse(strpos($stylesheet, $needle) > -1);
    $this->assertTrue(strpos($stylesheet, $new_needle) > -1);

    // Check that the library_info_alter works.
    $library = \Drupal::service('library.discovery')->getLibraryByName('crossword', 'crossword.default');
    $this->assertEquals('public://crossword-colors.css', $library['css'][3]['data'], json_encode($library));
  }

  /**
   * Load library without ever visiting form.
   */
  public function testCrosswordColorsInstall() {
    $this->assertFileDoesNotExist('public://crossword-colors.css');
    $this->drupalLogin($this->drupalCreateUser(['configure crossword colors']));
    $library = \Drupal::service('library.discovery')->getLibraryByName('crossword', 'crossword.default');
    // The library_info_alter hook should have created the file.
    $this->assertFileExists('public://crossword-colors.css');
    $this->assertEquals('public://crossword-colors.css', $library['css'][3]['data'], json_encode($library));
    // Check the default config was used to make the file.
    $stylesheet = file_get_contents('public://crossword-colors.css');
    $needle = '.crossword-square.active {background-color: #1892ef;}';
    $this->assertTrue(strpos($stylesheet, $needle) > -1);
  }

}
