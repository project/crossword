<?php

namespace Drupal\Tests\crossword\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests validation of Crossword field.
 *
 * This does not test ReferenceAccess and FileValidation, which are Validators
 * provided by core.
 *
 * @group crossword
 */
class CrosswordFieldValidateTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['crossword', 'crossword_tests'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests validation of file structure.
   *
   * Note that the parsers themselves are tested more thoroughly in other tests.
   */
  public function testCrosswordFileValdator() {
    // Not crossword.
    $filename = 'failure.txt';
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/" . $filename);
    $not_crossword = \Drupal::service('file.repository')->writeData($contents, "public://$filename");

    // Corrupted txt.
    $filename = 'corrupted_bad_grid.txt';
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/" . $filename);
    $corrupted_txt = \Drupal::service('file.repository')->writeData($contents, "public://$filename");

    // Ok txt.
    $filename = 'test.txt';
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/" . $filename);
    $ok_txt = \Drupal::service('file.repository')->writeData($contents, "public://$filename");

    // Ok puz.
    $filename = 'test.puz';
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/" . $filename);
    $ok_puz = \Drupal::service('file.repository')->writeData($contents, "public://$filename");

    // Log in as admin.
    $this->drupalLogin($this->drupalCreateUser([], 'radmin', TRUE));

    $this->drupalGet('/node/add/crossword');
    $page = $this->getSession()->getPage();
    $page->attachFileToField('files[field_crossword_0]', $not_crossword->getFileUri());
    $this->submitForm([], 'Upload');
    $error_message = t('That does not appear to be a supported Crossword Puzzle file format.');
    $this->assertSession()->responseContains('Error message');
    $this->assertSession()->responseContains($error_message);

    $this->submitForm([], 'Remove');
    $page->attachFileToField('files[field_crossword_0]', $corrupted_txt->getFileUri());
    $this->submitForm([], 'Upload');
    $error_message = t('The file format was identified as %parser, but the file reader was unsuccessful. The file may be corrupted or contain unsupported features. The message was: %message', ['%parser' => 'Across Lite Text', '%message' => 'The grid is not rectangular.']);
    $this->assertSession()->responseContains('Error message');
    $this->assertSession()->responseContains($error_message);

    $this->submitForm([], 'Remove');
    $page->attachFileToField('files[field_crossword_0]', $ok_txt->getFileUri());
    $this->submitForm([], 'Upload');
    $this->assertSession()->responseNotContains('Error message');

    $this->submitForm([], 'Remove');
    $page->attachFileToField('files[field_crossword_0]', $ok_puz->getFileUri());
    $this->submitForm([], 'Upload');
    $this->assertSession()->responseNotContains('Error message');
  }

  /**
   * Tests validation of dimensions.
   */
  public function testCrosswordDimensionsValdator() {
    // Big file (25x25).
    $filename = '3185956.puz';
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/" . $filename);
    $big_file = \Drupal::service('file.repository')->writeData($contents, "public://$filename");

    // Tiny File (2x2).
    $filename = 'tiny.txt';
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/" . $filename);
    $tiny_file = \Drupal::service('file.repository')->writeData($contents, "public://$filename");

    // Ok size (3x3).
    $filename = 'test.txt';
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/" . $filename);
    $ok_file = \Drupal::service('file.repository')->writeData($contents, "public://$filename");

    // Log in as admin.
    $this->drupalLogin($this->drupalCreateUser([], 'radmin', TRUE));

    $this->drupalGet('/node/add/crossword');
    $page = $this->getSession()->getPage();
    $page->attachFileToField('files[field_crossword_0]', $big_file->getFileUri());
    $this->submitForm([], 'Upload');
    $error_message = t('That puzzle has %columns columns, which is more than the allowed %allowed.', ['%columns' => '25', '%allowed' => '15']);
    $this->assertSession()->responseContains('Error message');
    $this->assertSession()->responseContains($error_message);

    $this->submitForm([], 'Remove');
    $page->attachFileToField('files[field_crossword_0]', $tiny_file->getFileUri());
    $this->submitForm([], 'Upload');
    $error_message = t('That puzzle has %columns columns, which is fewer than the required %required.', ['%columns' => '2', '%required' => '3']);
    $this->assertSession()->responseContains('Error message');
    $this->assertSession()->responseContains($error_message);

    $this->submitForm([], 'Remove');
    $page->attachFileToField('files[field_crossword_0]', $ok_file->getFileUri());
    $this->submitForm([], 'Upload');
    $this->assertSession()->responseNotContains('Error message');
  }

}
