<?php

namespace Drupal\Tests\crossword\Functional;

use Drupal\media\Entity\Media;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests crossword_contest security.
 *
 * @group crossword
 */
class CrosswordContestAccessTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['crossword_contest_tests'];

  /**
   * {@inheritdoc}
   */
  public $defaultTheme = 'stark';

  /**
   * Name of test puzzle.
   *
   * @var string
   */
  protected $testPuzzleFilename = 'test.txt';

  /**
   * Test access on the route.
   */
  public function testCrosswordContestAccess() {
    $this->createTestMedia();
    $answer = urlencode(json_encode(['answer']));

    // Published media access.
    $this->drupalLogin($this->drupalCreateUser([
      'access content',
      'view media',
    ]));
    // Published.
    $this->drupalGet("crossword-contest/1/$answer");
    $this->assertSession()->statusCodeEquals(200);
    // Unpublished.
    $this->drupalGet("crossword-contest/2/$answer");
    $this->assertSession()->statusCodeEquals(403);
    // No crossword.
    $this->drupalGet("crossword-contest/3/$answer");
    $this->assertSession()->statusCodeEquals(403);
    // Media doesn't exist.
    $this->drupalGet("crossword-contest/4/$answer");
    $this->assertSession()->statusCodeEquals(403);

    // Now as admin.
    $this->drupalLogin($this->drupalCreateUser([], 'new_admin', TRUE));
    $this->drupalGet("crossword-contest/1/$answer");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("crossword-contest/2/$answer");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("crossword-contest/3/$answer");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("crossword-contest/4/$answer");
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Helper function to create media.
   */
  protected function createTestMedia() {
    // First we move a test file to the file system.
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/{$this->testPuzzleFilename}");
    $file = \Drupal::service('file.repository')->writeData($contents, "public://{$this->testPuzzleFilename}");
    // Now use that file in a new crossword_contests media.
    $media = Media::create(['bundle' => 'crossword_contest']);
    $media->set('name', 'Published Crossword Contest');
    $media->set('field_crossword_contest_xword', $file->id());
    $media->set('status', 1);
    $media->save();
    $media = Media::create(['bundle' => 'crossword_contest']);
    $media->set('name', 'Un-Published Crossword Contest');
    $media->set('field_crossword_contest_xword', $file->id());
    $media->set('status', 0);
    $media->save();
    $media = Media::create(['bundle' => 'crossword_contest']);
    $media->set('name', 'Sans Crossword Contest');
    $media->set('status', 1);
    $media->save();
  }

}
