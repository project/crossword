<?php

namespace Drupal\Tests\crossword\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests crossword_instructions block plugin.
 *
 * @group crossword
 */
class CrosswordInstructionsBlockTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['crossword', 'block'];

  /**
   * Test the block settings.
   */
  public function testCrosswordInstructionsBlock() {
    $this->drupalPlaceBlock('crossword_instructions',
      [
        'cheat' => FALSE,
        'rebus' => FALSE,
        'errors' => FALSE,
      ]
    );
    $this->drupalGet('<front>');
    $this->assertSession()->pageTextContains('Active Square');
    $this->assertSession()->pageTextNotContains('Cheat');
    $this->assertSession()->pageTextNotContains('Errors');
    $this->assertSession()->pageTextNotContains('Rebus');

    // Set everything to true in a new block.
    $this->drupalPlaceBlock('crossword_instructions',
      [
        'cheat' => TRUE,
        'rebus' => TRUE,
        'errors' => TRUE,
      ]
    );
    $this->drupalGet('<front>');
    $this->assertSession()->pageTextContains('Cheat');
    $this->assertSession()->pageTextContains('Errors');
    $this->assertSession()->pageTextContains('Rebus');
  }

}
