<?php

namespace Drupal\Tests\crossword\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests regenerating crossword images.
 *
 * This happens in a batch executed from a form, so it has to be a
 * functional test.
 *
 * @group crossword
 */
class CrosswordImageRegenerateTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['crossword_image'];

  /**
   * {@inheritdoc}
   */
  public $defaultTheme = 'stark';

  /**
   * Test regeneration triggered through the form.
   */
  public function testRegenerate() {
    // Create a crossword file.
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/test.txt");
    $file = \Drupal::service('file.repository')->writeData($contents, "public://test.txt");

    // Create a couple crossword images. Don't create solution.
    $crossword_image_service = \Drupal::service('crossword.image_service');
    $thumbnail = $crossword_image_service->getImageEntity($file, 'thumbnail');
    $thumbnail_time = filemtime($thumbnail->getFileUri());
    $numbered = $crossword_image_service->getImageEntity($file, 'numbered_thumbnail');
    $numbered_time = filemtime($numbered->getFileUri());
    // Confirm they exist.
    $this->assertFileExists('public://crossword/1-thumbnail.png');
    $this->assertFileExists('public://crossword/1-numbered_thumbnail.png');
    $this->assertFileDoesNotExist('public://crossword/1-solution_thumbnail.png');
    // Put in a delay so updates can be measured..
    sleep(2);
    // Navigate to the regenerate form.
    $this->drupalLogin($this->drupalCreateUser(['administer crossword images']));
    $this->drupalGet('/admin/config/media/crossword/regenerate');
    $this->assertSession()->pageTextContains('Thumbnail');
    $this->assertSession()->pageTextContains('Numbered Thumbnail');
    $this->assertSession()->pageTextContains('Solution Thumbnail');
    $selections = [
      'crossword_image_plugin_ids[thumbnail]' => 0,
      'crossword_image_plugin_ids[numbered_thumbnail]' => 'numbered_thumbnail',
      'crossword_image_plugin_ids[solution_thumbnail]' => 0,
    ];
    $this->submitForm($selections, 'Regenerate Crossword Images');
    // Batch has presumably run.
    $this->assertSession()->pageTextContains('Crossword Image Regeneration is complete');
    // Check file existence (or non-existence).
    $this->assertFileExists('public://crossword/1-thumbnail.png');
    $this->assertFileDoesNotExist('public://crossword/1-thumbnail_0.png');
    $this->assertFileExists('public://crossword/1-numbered_thumbnail.png');
    $this->assertFileDoesNotExist('public://crossword/1-numbered_thumbnail_0.png');
    $this->assertFileDoesNotExist('public://crossword/1-solution_thumbnail.png');
    // Check modification times. Only numbered should have been updated.
    $this->assertEquals($thumbnail_time, filemtime('public://crossword/1-thumbnail.png'));
    $this->assertGreaterThan($numbered_time, filemtime('public://crossword/1-numbered_thumbnail.png'));
  }

}
