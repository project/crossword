<?php

namespace Drupal\Tests\crossword\Kernel;

/**
 * Tests the Crossword Compiler Xml parser plugin.
 *
 * @group crossword
 */
class CrosswordCompilerXmlParserTest extends CrosswordFileParserPluginTestBase {

  /**
   * {@inheritdoc}
   */
  public $pluginId = 'crossword_compiler_xml';

  /**
   * {@inheritdoc}
   */
  public $class = 'Drupal\crossword\Plugin\crossword\crossword_file_parser\CrosswordCompilerXmlParser';

  /**
   * {@inheritdoc}
   */
  public $filename = [
    'success' => 'test.xml',
    'failure' => 'failure.txt',
  ];

  /**
   * Provider for testExceptions().
   *
   * @return array
   *   Structured data for tests.
   */
  public static function exceptionsProvider() {
    return [
      'Irregular grid' => [
        'filename' => 'corrupted_bad_grid.xml',
        'message' => 'The grid is not rectangular.',
      ],
      'Missing down clue' => [
        'filename' => 'corrupted_missing_down_clue.xml',
        'message' => 'Number of down clues does not match size of grid.',
      ],
      'Missing across clue' => [
        'filename' => 'corrupted_missing_across_clue.xml',
        'message' => 'Number of across clues does not match size of grid.',
      ],
      'No clues' => [
        'filename' => 'corrupted_no_clues.xml',
        'message' => 'There are no clues.',
      ],
      'Extra across clue' => [
        'filename' => 'corrupted_extra_across_clue.xml',
        'message' => 'Number of across clues does not match size of grid.',
      ],
      'Extra down clue' => [
        'filename' => 'corrupted_extra_down_clue.xml',
        'message' => 'Number of down clues does not match size of grid.',
      ],
      'Corrupted image' => [
        'filename' => '3173956_corrupted_image.xml',
        'message' => 'Puzzle contains a corrupted image.',
      ],
      'Valid example' => [
        'filename' => '3173956.xml',
        'message' => NULL,
      ],
    ];
  }

}
