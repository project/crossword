<?php

namespace Drupal\Tests\crossword\Kernel;

/**
 * Tests the Across Lite Text parser plugin.
 *
 * @group crossword
 */
class AcrossLiteTextParserTest extends CrosswordFileParserPluginTestBase {

  /**
   * {@inheritdoc}
   */
  public $pluginId = 'across_lite_text';

  /**
   * {@inheritdoc}
   */
  public $class = 'Drupal\crossword\Plugin\crossword\crossword_file_parser\AcrossLiteTextParser';

  /**
   * {@inheritdoc}
   */
  public $filename = [
    'success' => 'test.txt',
    'failure' => 'failure.txt',
  ];

  /**
   * Provider for testExceptions().
   *
   * @return array
   *   Structured data for tests.
   */
  public static function exceptionsProvider() {
    return [
      'Irregular grid' => [
        'filename' => 'corrupted_bad_grid.txt',
        'message' => 'The grid is not rectangular.',
      ],
      'Missing down clue' => [
        'filename' => 'corrupted_missing_down_clue.txt',
        'message' => 'Number of down clues does not match size of grid.',
      ],
      'Missing across clue' => [
        'filename' => 'corrupted_missing_across_clue.txt',
        'message' => 'Number of across clues does not match size of grid.',
      ],
      'No clues' => [
        'filename' => 'corrupted_no_clues.txt',
        'message' => 'The across clues are missing.',
      ],
      'Extra across clue' => [
        'filename' => 'corrupted_extra_across_clue.txt',
        'message' => 'Number of across clues does not match size of grid.',
      ],
      'Extra down clue' => [
        'filename' => 'corrupted_extra_down_clue.txt',
        'message' => 'Number of down clues does not match size of grid.',
      ],
      'Valid example' => [
        'filename' => 'example.txt',
        'message' => NULL,
      ],
      'Issue 3102646: Indented lines' => [
        'filename' => '3102646.txt',
        'message' => NULL,
      ],
    ];
  }

}
