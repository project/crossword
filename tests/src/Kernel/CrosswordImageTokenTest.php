<?php

namespace Drupal\Tests\crossword\Kernel;

use Drupal\image\Entity\ImageStyle;
use Drupal\Tests\token\Kernel\KernelTestBase;

/**
 * Tests crossword image tokens.
 *
 * @requires module token
 * @group crossword
 */
class CrosswordImageTokenTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'file',
    'image',
    'crossword',
    'crossword_image',
    'crossword_token',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('file');
    $this->installSchema('file', ['file_usage']);
  }

  /**
   * Test the crossword image tokens.
   */
  public function testCrosswordImageTokens() {
    // Create a test crossword file.
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/test.txt");
    $file = \Drupal::service('file.repository')->writeData($contents, "public://test.txt");
    $expected_uri = \Drupal::service('crossword.image_service')->getImageUri($file, 'thumbnail');
    $expected_thumbnail_url = \Drupal::service('file_url_generator')->generateAbsoluteString($expected_uri);
    $style = ImageStyle::create(
      [
        'name' => 'test_style',
        'label' => 'test_style',
      ]
    );
    $style->save();
    $expected_style_url = $style->buildUrl($expected_uri);
    $tokens = [
      'crossword_image' => NULL,
      'crossword_image:thumbnail' => $expected_thumbnail_url,
      'crossword_image:thumbnail:test_style' => $expected_style_url,
      'crossword_image:thumbnail:test_style:width' => NULL,
      'crossword_image:thumbnail:bad_image_style' => NULL,
    ];
    $this->assertTokens('file', ['file' => $file], $tokens);

    // Test a file that is not a crossword.
    $fake_file = \Drupal::service('file.repository')->writeData('Fake!', "public://fake.txt");
    $tokens = [
      'crossword_image' => NULL,
      'crossword_image:thumbnail' => NULL,
      'crossword_image:thumbnail:test_style' => NULL,
      'crossword_image:thumbnail:test_style:width' => NULL,
      'crossword_image:thumbnail:bad_image_style' => NULL,
    ];
    $this->assertNoTokens('file', ['file' => $fake_file], $tokens);
  }

}
