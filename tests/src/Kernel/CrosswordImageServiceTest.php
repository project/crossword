<?php

namespace Drupal\Tests\crossword\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Covers CrosswordImageService.
 *
 * @group crossword
 */
class CrosswordImageServiceTest extends KernelTestBase {

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'crossword',
    'crossword_image',
    'file',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['file', 'user']);
    $this->installSchema('user', ['users_data']);
    $this->installSchema('file', ['file_usage']);
    $this->installEntitySchema('user');
    $this->installEntitySchema('file');
  }

  /**
   * Test that image service creates file with correct uri.
   */
  public function testCrosswordImageService() {
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/test.txt");
    $file = \Drupal::service('file.repository')->writeData($contents, "public://test.txt");

    $crossword_image_manager = \Drupal::service('crossword.manager.image');
    $plugin_ids = array_keys($crossword_image_manager->getCrosswordImageOptionList());

    $file_usage = \Drupal::service('file.usage');
    $expected_usage = [
      'crossword' => [
        'file' => [
          '1' => '1',
        ],
      ],
    ];

    $crossword_image_service = \Drupal::service('crossword.image_service');
    /*
     * An image should be created when either getImageUri or getImageEntity
     * is called. We want to confirm that the service works regardless of
     * which function is called.
     */
    // On the first plugin, call getImageUri on the plugin.
    $plugin_id = $plugin_ids[0];
    $image_uri = $crossword_image_service->getImageUri($file, $plugin_id);
    $this->assertEquals("public://crossword/1-{$plugin_id}.png", $image_uri);

    // Now for all the plugins call getImageEntity.
    foreach ($plugin_ids as $plugin_id) {
      $image = $crossword_image_service->getImageEntity($file, $plugin_id);
      $this->assertEquals("public://crossword/1-{$plugin_id}.png", $image->getFileUri());
      $this->assertEquals($expected_usage, $file_usage->listUsage($image));
    }
  }

}
