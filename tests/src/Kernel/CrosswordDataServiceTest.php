<?php

namespace Drupal\Tests\crossword\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Covers CrosswordDataService.
 *
 * @group crossword
 */
class CrosswordDataServiceTest extends KernelTestBase {

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  protected static $modules = ['system', 'crossword', 'file', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['file', 'user']);
    $this->installSchema('user', ['users_data']);
    $this->installSchema('file', ['file_usage']);
    $this->installEntitySchema('user');
    $this->installEntitySchema('file');
  }

  /**
   * Test data service against test puzzle.
   */
  public function testCrosswordDataService() {
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/test.txt");
    $file = \Drupal::service('file.repository')->writeData($contents, "public://test.txt");
    $expected_json = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/test.json");
    $expected_data = json_decode($expected_json, TRUE);
    $expected_data['id'] = $file->id();

    // Not cached at first.
    $cache_service = \Drupal::service('cache.crossword');
    $this->assertFalse($cache_service->get($file->id()));

    // Test the big method.
    $crossword_data_service = \Drupal::service('crossword.data_service');
    $data = $crossword_data_service->getData($file);
    $this->assertEquals($expected_data, $data);

    // Now it should be cached.
    $cached = $cache_service->get($file->id());
    $this->assertNotEmpty($cached);
    $this->assertEquals($expected_data, $cached->data['data']);

    // Check all the other methods.
    $expected_solution = [['A', '', 'ONE'], ['B', 'C', 'D'], ['TWO', 'E', 'F']];
    $solution = $crossword_data_service->getSolution($file);
    $this->assertEquals($expected_solution, $solution);
    $solution_dot = [['A', '.', 'ONE'], ['B', 'C', 'D'], ['TWO', 'E', 'F']];
    $solution = $crossword_data_service->getSolution($file, '.');
    $this->assertEquals($solution_dot, $solution);

    $title = $crossword_data_service->getTitle($file);
    $this->assertEquals('Test Puzzle', $title);

    $author = $crossword_data_service->getAuthor($file);
    $this->assertEquals('Test Author', $author);

    $dimensions = $crossword_data_service->getDimensions($file, ' by ');
    $this->assertEquals('3 by 3', $dimensions);

    $dimension_across = $crossword_data_service->getDimensionAcross($file);
    $this->assertEquals(3, $dimension_across);

    $dimension_down = $crossword_data_service->getDimensionDown($file);
    $this->assertEquals(3, $dimension_down);

    $is_rebus = $crossword_data_service->isRebus($file);
    $this->assertTrue($is_rebus);

    // Test get redacted data.
    $redacted_json = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/redacted.json");
    $redacted_data = json_decode($redacted_json, TRUE);
    $redacted_data['id'] = $file->id();
    $data = $crossword_data_service->getData($file, TRUE);
    $this->assertEquals($redacted_data, $data);
  }

  /**
   * Test data service against xss attempts.
   *
   * We explicitly test that script tags and onclick get stripped.
   */
  public function testCrosswordDataServiceXss() {
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/xss.txt");
    $file = \Drupal::service('file.repository')->writeData($contents, "public://xss.txt");
    $expected_json = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/xss.json");
    $expected_data = json_decode($expected_json, TRUE);
    $expected_data['id'] = $file->id();

    $crossword_data_service = \Drupal::service('crossword.data_service');
    $data = $crossword_data_service->getData($file);
    $this->assertEquals($expected_data, $data);
  }

  /**
   * Tests that json_encode works, which requires proper UTF-8 encoding.
   *
   * The file we parse is from issue 3102647 which spun off of 3102499.
   */
  public function testJsonEncoding() {
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/3102647.puz");
    $file = \Drupal::service('file.repository')->writeData($contents, "public://3102647.puz");

    $crossword_data_service = \Drupal::service('crossword.data_service');
    $data = $crossword_data_service->getData($file);
    $this->assertNotEmpty(json_encode($data));
  }

  /**
   * Tests that a corrupted file is handled correctly, especially caching.
   */
  public function testCorruptedFile() {
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/corrupted_bad_grid.puz");
    $file = \Drupal::service('file.repository')->writeData($contents, "public://corrupted_bad_grid.puz");

    $cache_service = \Drupal::service('cache.crossword');
    $this->assertFalse($cache_service->get($file->id()));
    $crossword_data_service = \Drupal::service('crossword.data_service');
    $data = $crossword_data_service->getData($file);
    $this->assertNull($data);
    $cached = $cache_service->get($file->id());
    $this->assertNotEmpty($cached);
    $this->assertNull($cached->data['data']);
    $data = $crossword_data_service->getData($file);
    $this->assertNull($data);
  }

}
