<?php

namespace Drupal\Tests\crossword\Kernel;

/**
 * Tests the Ipuz parser plugin.
 *
 * @group crossword
 */
class IpuzParserTest extends CrosswordFileParserPluginTestBase {

  /**
   * {@inheritdoc}
   */
  public $pluginId = 'ipuz';

  /**
   * {@inheritdoc}
   */
  public $class = 'Drupal\crossword\Plugin\crossword\crossword_file_parser\IpuzParser';

  /**
   * {@inheritdoc}
   */
  public $filename = [
    'success' => 'test.ipuz',
    'failure' => 'failure.txt',
  ];

  /**
   * Provider for testExceptions().
   *
   * @return array
   *   Structured data for tests.
   */
  public static function exceptionsProvider() {
    return [
      'Irregular grid' => [
        'filename' => 'corrupted_bad_grid.ipuz',
        'message' => 'The grid is not rectangular.',
      ],
      'Missing down clue' => [
        'filename' => 'corrupted_missing_down_clue.ipuz',
        'message' => 'Number of down clues does not match size of grid.',
      ],
      'Missing across clue' => [
        'filename' => 'corrupted_missing_across_clue.ipuz',
        'message' => 'Number of across clues does not match size of grid.',
      ],
      'No clues' => [
        'filename' => 'corrupted_no_clues.ipuz',
        'message' => 'There are no clues.',
      ],
      'Extra across clue' => [
        'filename' => 'corrupted_extra_across_clue.ipuz',
        'message' => 'Number of across clues does not match size of grid.',
      ],
      'Extra down clue' => [
        'filename' => 'corrupted_extra_down_clue.ipuz',
        'message' => 'Number of down clues does not match size of grid.',
      ],
      'Just the version string' => [
        'filename' => 'corrupted_just_enough_to_apply.ipuz',
        'message' => 'The file is corrupted.',
      ],
      'Valid example' => [
        'filename' => 'ipuz_dot_org_slash_example.ipuz',
        'message' => NULL,
      ],
    ];
  }

}
