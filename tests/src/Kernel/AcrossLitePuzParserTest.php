<?php

namespace Drupal\Tests\crossword\Kernel;

/**
 * Tests the Across Lite Puz parser plugin.
 *
 * @group crossword
 */
class AcrossLitePuzParserTest extends CrosswordFileParserPluginTestBase {

  /**
   * {@inheritdoc}
   */
  public $pluginId = 'across_lite_puz';

  /**
   * {@inheritdoc}
   */
  public $class = 'Drupal\crossword\Plugin\crossword\crossword_file_parser\AcrossLitePuzParser';

  /**
   * {@inheritdoc}
   */
  public $filename = [
    'success' => 'test.puz',
    'failure' => 'failure.txt',
  ];

  /**
   * Provider for testExceptions().
   *
   * @return array
   *   Structured data for tests.
   */
  public static function exceptionsProvider() {
    return [
      'Bad grid' => [
        'filename' => 'corrupted_bad_grid.puz',
        'message' => 'Number of clues does not match structure of grid.',
      ],
      'Just enough to apply' => [
        'filename' => 'corrupted_just_enough_to_apply.puz',
        'message' => 'The crossword file is corrupted.',
      ],
      'Clue list deleted' => [
        'filename' => 'corrupted_clue_list_deleted.puz',
        'message' => 'Number of clues does not match structure of grid.',
      ],
      'Valid example' => [
        'filename' => 'example.puz',
        'message' => NULL,
      ],
      'Issue 3185956: Lots of clues' => [
        'filename' => '3185956.puz',
        'message' => NULL,
      ],
      'Issue 3102647: Convert to UTF-8' => [
        // This issue is covered by CrosswordDataServiceTest::testJsonEncode().
        // We have the file though so we might as well parse it here.
        'filename' => '3102647.puz',
        'message' => NULL,
      ],
    ];
  }

}
