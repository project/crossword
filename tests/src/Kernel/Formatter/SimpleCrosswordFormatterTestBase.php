<?php

namespace Drupal\Tests\crossword\Kernel\Formatter;

use Drupal\entity_test\Entity\EntityTestRev;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;

/**
 * Base class for testing crossword field formatter plugins.
 *
 * These check that the render array gives uus what we expect.
 * See FunctionalJavascript tests for tests that test js.
 * Inspired by the core StringFormatterTest.
 */
abstract class SimpleCrosswordFormatterTestBase extends KernelTestBase {

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  protected static $modules = [
    'field',
    'entity_test',
    'system',
    'user',
    'file',
    'crossword',
  ];

  /**
   * Id of the plugin.
   *
   * @var string
   */
  public $id;

  /**
   * A test entity.
   *
   * @var Drupal\entity_test\Entity\EntityTestRev
   */
  public $entity;

  /**
   * The class of the formatter.
   *
   * @var string
   */
  protected $class;

  /**
   * Settings to try in addition to default settings.
   *
   * @var array
   */
  public $customSettings;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['system', 'field']);
    $this->installEntitySchema('entity_test_rev');
    $this->installSchema('file', ['file_usage']);
    $this->installEntitySchema('file');
    $this->entity = $this->createTestEntity();
    $this->class = \Drupal::service('plugin.manager.field.formatter')->getDefinitions()[$this->id]['class'];
  }

  /**
   * Helper function to create node that can be used for testing.
   *
   * @return Drupal\entity_test\Entity\EntityTestRev
   *   An entity to be used for testing.
   */
  protected function createTestEntity() {
    // First we move a test file to the file system.
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/test.txt");

    $file = \Drupal::service('file.repository')->writeData($contents, 'public://test.txt');
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_crossword',
      'entity_type' => 'entity_test_rev',
      'type' => 'crossword',
    ]);
    $field_storage->save();

    $instance = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'entity_test_rev',
      'label' => 'Crossword',
    ]);
    $instance->save();

    $entity = EntityTestRev::create([]);
    $entity->set('field_crossword', $file->id());
    $entity->save();
    return $entity;
  }

  /**
   * Test things in the details section: title, author, notepad.
   *
   * @return bool
   *   True if everything looks good.
   */
  protected function checkDetail(string $detail_name, array $render, string $value, string $html_tag) {
    if (empty($html_tag)) {
      return empty($render[0]['#content'][$detail_name]);
    }
    return ($value == $render[0]['#content'][$detail_name]['#value']) && ($html_tag == $render[0]['#content'][$detail_name]['#tag']);
  }

}
