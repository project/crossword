<?php

namespace Drupal\Tests\crossword\Kernel\Formatter;

use Drupal\Core\Url;

/**
 * Test the crossword_file_download_link formatter.
 *
 * This copies heavily off of tests in file_download_link.
 *
 * @group crossword
 * @requires module file_download_link
 * @requires module token
 */
class CrosswordImageDownloadLinkFormatterTest extends SimpleCrosswordFormatterTestBase {

  /**
   * {@inheritdoc}
   */
  public $id = 'crossword_image_download';

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  protected static $modules = [
    'field',
    'system',
    'user',
    'node',
    'file',
    'image',
    'entity_test',
    'token',
    'crossword',
    'crossword_image',
    'crossword_token',
    'crossword_download',
  ];

  /**
   * Test the formatter using a token from the file.
   */
  public function testFormatter() {
    $settings = [
      'link_text' => 'Solution ([file:extension])',
      'new_tab' => FALSE,
      'force_download' => FALSE,
      'crossword_image' => 'solution_thumbnail',
    ];
    $render = $this->entity->field_crossword->view([
      'type' => 'crossword_image_download',
      'label' => 'hidden',
      'settings' => $settings,
    ]);
    $expected_render = [
      '#type' => 'link',
      '#title' => 'Solution (png)',
      '#url' => \Drupal::service('file_url_generator')->generate('public://crossword/1-solution_thumbnail.png'),
      '#options' => [
        'attributes' => [
          'class' => [
            'file-download',
            'file-download-image',
            'file-download-png',
          ],
        ],
      ],
      '#cache' => [
        'tags' => [
          'entity_test_rev:1',
          'file:1',
          'file:2',
        ],
        'contexts' => [],
        'max-age' => -1,
      ],
      '#attached' => [],
    ];

    $this->assertEqualsCanonicalizing($expected_render, $render[0]);
  }

}
