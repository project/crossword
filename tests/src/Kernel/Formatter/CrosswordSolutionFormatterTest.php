<?php

namespace Drupal\Tests\crossword\Kernel\Formatter;

/**
 * Test the crossword_solution formatter.
 *
 * @group crossword
 */
class CrosswordSolutionFormatterTest extends SimpleCrosswordFormatterTestBase {

  /**
   * {@inheritdoc}
   */
  public $id = 'crossword_solution';

  /**
   * {@inheritdoc}
   */
  public $customSettings = [
    [
      'details' => [
        'title_tag' => 'h5',
        'author_tag' => 'p',
        'notepad_tag' => 'h2',
      ],
    ],
    [
      'details' => [
        'title_tag' => '',
        'author_tag' => '',
        'notepad_tag' => '',
      ],
    ],
  ];

  /**
   * Test the formatter using various settings.
   */
  public function testFormatter() {
    // These are the values in the test puzzle.
    $detail_values = [
      'title' => 'Test Puzzle',
      'author' => 'Test Author',
      'notepad' => 'This is a test.',
    ];
    // Get an array of settings sets together.
    $default_settings = $this->class::defaultSettings();
    $settings_array[] = $default_settings;
    if (!empty($this->customSettings)) {
      $settings_array = array_merge($settings_array, $this->customSettings);
    }

    // Test the formatter with each array of settings.
    foreach ($settings_array as $settings) {
      $render = $this->entity->field_crossword->view([
        'type' => $this->id,
        'label' => 'hidden',
        'settings' => $settings,
      ]);
      $this->assertTrue(!empty($render), "The render array is empty");
      foreach ($settings['details'] as $key => $tag) {
        $detail_name = explode("_", $key)[0];
        $this->assertTrue($this->checkDetail($detail_name, $render, $detail_values[$detail_name], $tag), "The $detail_name detail is wrong.");
      }
    }
  }

}
