<?php

namespace Drupal\Tests\crossword\Kernel\Formatter;

use Drupal\Core\Url;

/**
 * Test the crossword_file_download_link formatter.
 *
 * This copies heavily off of tests in file_download_link.
 *
 * @group crossword
 * @requires module file_download_link
 * @requires module token
 */
class CrosswordFileDownloadLinkFormatterTest extends SimpleCrosswordFormatterTestBase {

  /**
   * {@inheritdoc}
   */
  public $id = 'crossword_file_download_link';

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  protected static $modules = [
    'field',
    'system',
    'user',
    'node',
    'file',
    'image',
    'entity_test',
    'token',
    'crossword',
    'crossword_token',
    'crossword_download',
  ];

  /**
   * Test the formatter using a token from the file.
   */
  public function testFormatter() {
    $settings = [
      'link_text' => '[file:crossword_title] ([file:crossword_dimensions])',
      'new_tab' => FALSE,
      'force_download' => FALSE,
    ];
    $render = $this->entity->field_crossword->view([
      'type' => 'crossword_file_download_link',
      'label' => 'hidden',
      'settings' => $settings,
    ]);
    $expected_render = [
      '#type' => 'link',
      '#title' => 'Test Puzzle (3x3)',
      '#url' => \Drupal::service('file_url_generator')->generate('public://test.txt'),
      '#options' => [
        'attributes' => [
          'class' => [
            'file-download',
            'file-download-text',
            'file-download-plain',
          ],
        ],
      ],
      '#cache' => [
        'tags' => [
          'entity_test_rev:1',
          'file:1',
        ],
        'contexts' => [],
        'max-age' => -1,
      ],
      '#attached' => [],
    ];

    $this->assertEqualsCanonicalizing($expected_render, $render[0]);
  }

}
