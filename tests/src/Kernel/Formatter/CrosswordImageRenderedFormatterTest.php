<?php

namespace Drupal\Tests\crossword\Kernel\Formatter;

/**
 * Test the crossword_image_rendered formatter.
 *
 * @group crossword
 */
class CrosswordImageRenderedFormatterTest extends SimpleCrosswordFormatterTestBase {

  /**
   * {@inheritdoc}
   */
  public $id = 'crossword_image_rendered';

  /**
   * {@inheritdoc}
   */
  public $customSettings = [
    ['crossword_image' => 'solution_thumbnail'],
    ['crossword_image' => 'numbered_thumbnail'],
  ];

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'entity_test',
    'system',
    'user',
    'file',
    'crossword',
    'image',
    'crossword_image',
  ];

  /**
   * {@inheritdoc}
   */
  public function testFormatter() {
    // Get an array of settings sets together.
    $default_settings = $this->class::defaultSettings();
    $settings_array[] = $default_settings;
    if (!empty($this->customSettings)) {
      $settings_array = array_merge($settings_array, $this->customSettings);
    }

    // Test the formatter with each array of settings.
    foreach ($settings_array as $settings) {
      $render = $this->entity->field_crossword->view([
        'type' => $this->id,
        'label' => 'hidden',
        'settings' => $settings,
      ]);
      $this->assertTrue(!empty($render), "The render array is empty");
      $expected_uri = 'public://crossword/1-' . $settings['crossword_image'] . '.png';
      $this->assertEquals($render[0]['#item']->uri, $expected_uri, "The image uri is not correct for {$settings['crossword_image']}.");
      $this->assertFileExists($expected_uri, "The {$settings['crossword_image']} does not exist.");
    }
  }

}
