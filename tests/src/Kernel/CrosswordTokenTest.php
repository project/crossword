<?php

namespace Drupal\Tests\crossword\Kernel;

use Drupal\Tests\token\Kernel\KernelTestBase;

/**
 * Tests file tokens.
 *
 * @requires module token
 * @group crossword
 */
class CrosswordTokenTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['file', 'crossword', 'crossword_token'];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('file');
  }

  /**
   * Test the crossword tokens.
   */
  public function testCrosswordTokens() {
    // Create a test crossword file.
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('crossword') . "/tests/files/test.txt");
    $file = \Drupal::service('file.repository')->writeData($contents, "public://test.txt");

    $tokens = [
      'crossword_title' => 'Test Puzzle',
      'crossword_author' => 'Test Author',
      'crossword_dimensions' => '3x3',
      'crossword_dimension_across' => 3,
      'crossword_dimension_down' => 3,
      'crossword_image:thumbnail' => NULL,
    ];
    $this->assertTokens('file', ['file' => $file], $tokens);

    // Test a file that is not a crossword.
    $fake_file = \Drupal::service('file.repository')->writeData('Fake!', "public://fake.txt");
    $tokens = [
      'crossword_title' => '',
      'crossword_author' => '',
      'crossword_dimensions' => '',
      'crossword_dimension_across' => '',
      'crossword_dimension_down' => '',
      'crossword_image:thumbnail' => NULL,
    ];
    $this->assertNoTokens('file', ['file' => $fake_file], $tokens);
  }

}
