CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

 This module makes it easy to add crossword puzzles that are playable in the
 browser to your Drupal site. It is not for authoring puzzles; rather, it
 allows you to upload crossword puzzle files that have been created elsewhere.

 A number of sub-modules add functionality.

 * crossword_media: Integrate with core media.
 * crossword_image: Create images out of crossword files.
 * crossword_token: Provides tokens for crossword files.
 * crossword_download: Offers field formatters related to downloading files.
 * crossword_pseudofields: Offers pseudofields for more flexibility.
 * crossword_status: Simple framework for client-side status tracking.
 * crossword_colors: Customize colors through the UI.
 * crossword_contest: Simple framework for low-stakes contest.

REQUIREMENTS
------------

 The main modules has no requirements outside of common core modules.
 Sub-modules may have additional requirements outside of core.

RECOMMENDED MODULES
-------------------

 The contrib token module is required by the crossword_token submodule.
 The contrib file_download_link module and token module are required
 for the crossword_download module.

 The contrib module view_mode_page might be useful for rendering a
 puzzle in different view modes on different pages. For example, a
 given crossword puzzle could reasonably be viewed on its own page
 using each of the Crossword Puzzle and Crossword Solution field formatters.

 The contrib module entity_print may be useful for creating pdfs out
 of a puzzle. Note however that the Crossword Module does not include
 any support for pdfs.

INSTALLATION
------------

 Install as usual, see https://www.drupal.org/node/1897420 for further
 information.

CONFIGURATION
-------------

 The only piece of this module with gloabel configuration is the
 crossword_pseudofields sub-module. Other configuration happens
 primarily at the level of Field Formatters.

 How To Render a Playable Puzzle
 1. Install the crossword module.
 2. Add a Crossword field to an entity (say a node type or media type
    called Puzzle).
 3. On the Manage Display form for the Puzzle, select the "Crossword
    Puzzle" formatter for the Crossword field.
 4. Create a new Puzzle using a crossword file in a supported format (Across
    Lite Text or Across Lite .puz or Crossword Compiler .xml or ipuz).
 5. View the Puzzle. You should see a fully functional puzzle.

 To easily add instructions to your site, use the Crossword Instructions Block
 provided by this module.

TROUBLESHOOTING
---------------

 Note that the only supported Crossword file formats are Across Lite Text
 and Across Lite Puz and limited support for Crossword Compiler XML and ipuz.
 Other formats could potentially be supported with new crossword_file_parser
 plugins, but they are not included in this module.

MAINTAINERS
-----------

 danflanagan8 (crossword enthusiast)
